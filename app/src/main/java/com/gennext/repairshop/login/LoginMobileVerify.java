package com.gennext.repairshop.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.ProfileActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.global.AppWebView;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.FieldValidation;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.ProgressButtonRounded;
import com.gennext.repairshop.util.RequestBuilder;

import java.util.Locale;


public class LoginMobileVerify extends CompactFragment {

    private EditText etOtp;
    //    private AssignTask assignTask;
    private ProgressButtonRounded btnSigninOtp;
    private LinearLayout llTearms;
    private String mMobile;
    private ApiRequest apiReq;
    private TextView tvTimerCount;
    private long TIME_IN_MILL_SEC = 120000;

    public static LoginMobileVerify newInstance(String mobile) {
        LoginMobileVerify fragment = new LoginMobileVerify();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        fragment.mMobile = mobile;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (apiReq != null) {
            apiReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (apiReq != null) {
            apiReq.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_login_verify, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        etOtp = (EditText) v.findViewById(R.id.et_login_verify);
        tvTimerCount = (TextView) v.findViewById(R.id.tv_timer_count);
        TextView tvOtp = (TextView) v.findViewById(R.id.tv_otp_send);
        tvOtp.setText(getString(R.string.mobile_authenticate_message2) + "\n+91-" + mMobile);

        btnSigninOtp = ProgressButtonRounded.newInstance(getContext(), v);

        btnSigninOtp.setText("Submit");

        btnSigninOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etOtp)) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etOtp.getText().toString());
            }
        });

        llTearms = (LinearLayout) v.findViewById(R.id.ll_tearm_user);

        llTearms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(AppWebView.newInstance("TEARMS OF USE", AppSettings.TERMS_OF_USE, AppWebView.URL), "appWebView");
            }
        });

        btnSigninOtp.setOnEditorActionListener(etOtp, "Done");
        startCountdownTimer();

    }

    private void startCountdownTimer() {
        CounterClass timer = new CounterClass(TIME_IN_MILL_SEC, 1000);
        timer.start();
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTimerCount.setVisibility(View.GONE);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            int seconds = (int) (millisUntilFinished / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            String time = String.format(Locale.US, "%02d", minutes)
                    + "min :" + String.format(Locale.US, "%02d", seconds);
            tvTimerCount.setText("Wait " + time + "sec");

        }

    }


    private void executeTask(final String otp) {

        apiReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                btnSigninOtp.startAnimation();
            }

            @Override
            public void onResponse(String response) {
                Model result = JsonParser.verifyOTP(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    AppUser.setMobile(getContext(), mMobile);
                    AppUser.setUserId(getContext(), result.getOutputMsg());
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnSigninOtp.revertAnimation();
                    PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    apiReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                btnSigninOtp.revertAnimation();
                executeTask(otp);
            }

        });
        apiReq.execute(ApiRequest.POST, AppSettings.VERIFY_OTP, RequestBuilder.loginMobile(mMobile, otp));
    }

    private void animateButtonAndRevert() {
        Handler handler = new Handler();


        Runnable runnableRevert = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        };

        btnSigninOtp.revertSuccessAnimation();
        handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
    }

}