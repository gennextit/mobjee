package com.gennext.repairshop.login;

/**
 * Created by Admin on 5/22/2017.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gennext.repairshop.LoginActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.global.AppWebView;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCall;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.FieldValidation;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.ProgressButtonRounded;
import com.gennext.repairshop.util.RequestBuilder;


public class LoginMobile extends CompactFragment {

    private EditText etMobile;
    private AssignTask assignTask;
    private ProgressButtonRounded btnAction;
    private LinearLayout llTearms;

    public static LoginMobile newInstance() {
        LoginMobile fragment = new LoginMobile();
        AppAnimation.setSlideAnimation(fragment, Gravity.RIGHT);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_login_mobile, container, false);
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {

        etMobile = (EditText) v.findViewById(R.id.et_login_mobile);

        btnAction = ProgressButtonRounded.newInstance(getContext(), v);

        btnAction.setText("Verify");
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!FieldValidation.isEmpty(getContext(), etMobile)) {
                    return;
                }
                hideKeybord(getActivity());
                executeTask(etMobile.getText().toString());
            }
        });

        llTearms=(LinearLayout)v.findViewById(R.id.ll_tearm_user);

        llTearms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(AppWebView.newInstance("TEARMS OF USE",AppSettings.TERMS_OF_USE, AppWebView.URL), "appWebView");
            }
        });

        btnAction.setOnEditorActionListener(etMobile,"Verify");

    }




    private void executeTask(String mobile) {
        assignTask = new AssignTask(getActivity(), mobile);
        assignTask.execute(AppSettings.LOGIN);
    }





    private class AssignTask extends AsyncTask<String, Void, Model> {
        private final String mobile;
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context, String mobile) {
            this.mobile = mobile;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnAction.startAnimation();
        }

        @Override
        protected Model doInBackground(String... urls) {
            String response = null;
            String macAddress = getMACAddress(getActivity());
            response = ApiCall.POST(urls[0], RequestBuilder.login(mobile,macAddress));
            return JsonParser.companyLogin(response);//return id=compId,data=name
        }


        @Override
        protected void onPostExecute(Model result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                if (result.getOutput().equals(Const.SUCCESS)) {
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    btnAction.revertAnimation();
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask(mobile);
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }

        private void animateButtonAndRevert() {
            Handler handler = new Handler();


            Runnable runnableRevert = new Runnable() {
                @Override
                public void run() {
                    if (context != null) {
                        ((LoginActivity)getActivity()).openVerifyScreen(mobile);
                    }
                }
            };

            btnAction.revertSuccessAnimation();
            handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
        }

    }

}