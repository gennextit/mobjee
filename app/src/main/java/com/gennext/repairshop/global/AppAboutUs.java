package com.gennext.repairshop.global;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.Sharewere;

/**
 * Created by Abhijit on 14-Nov-16.
 */
public class AppAboutUs extends CompactFragment {



    public static AppAboutUs newInstance() {
        AppAboutUs fragment=new AppAboutUs();
        AppAnimation.setDialogAnimation(fragment);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.app_about_us, container, false);
        initToolBar(getActivity(),v,"About Us");
        InitUI(v);
        return v;
    }

    private void InitUI(View v) {
        TextView tvAbout=(TextView)v.findViewById(R.id.tv_about_app);
        Button contactUs=(Button)v.findViewById(R.id.btn_contact_us);
        Button btnRateUs=(Button)v.findViewById(R.id.btn_rate_us);

        contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + Const.EMAIL_ID));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                }catch (ActivityNotFoundException e){
                    showToast(getContext(),"Please install an email app.");
                }
            }
        });

        btnRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sharewere.rateUs(getActivity());
            }
        });
    }
}
