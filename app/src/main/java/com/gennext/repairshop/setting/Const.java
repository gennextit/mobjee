package com.gennext.repairshop.setting;

/**
 * Created by Admin on 7/4/2017.
 */

public class Const {
    public static final int THEME_DARK = 3;
    public static final int THEME_LIGHT = 4;

    public static final int COLOR_STATUS_BAR = 1;
    public static final int COLOR_ACTION_BAR = 2;
    public static final int COLOR_SIDEMENU_ICON = 5;
    public static final int COLOR_SIDEMENU_TEXT = 6;
    public static final int COLOR_SIDEMENU_DIVIDER = 7;
    public static final int COLOR_DIALOG_DETAIL = 8;
    public static final int COLOR_DIALOG_BTN_ACTION = 9;
    public static final int COLOR_DIALOG_BTN_NORMAL = 10;

    public static final int DIMEN_MENU_PROFILE = 12;
    public static final int DIMEN_MENU_LIST = 13;
    public static final int DIMEN_DIALOG = 14;
    public static final long BUTTON_PROGRESS_TIME = 500;

    public static final String COMPLETE = "complete";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String SUBMIT = "Submit";
    public static final String RECORD_INSERTED = "Record Inserted";
    public static final String RECORD_UPDATED = "Record Updated";
    public static final String NO_RECORD_FOUND = "No record found";
    public static final String RECORD_ALREADY_EXISTS = "Record already exists";
    public static final String RECORD_DELETED_SUCCESSFUL = "Record deleted successful";
    public static final String RECORD_NOT_EXISTS = "Record not exists";
    public static final String FROM = "from";
    public static final String REPLIED_CHAT = "chat_replied";
    public static final String REPLIED_NOTIFICATION = "noti_replied";
    public static final String SEND_PHOTO = "Sent a Photo";
    public static final String SEND_LOCATION = "Sent a Location";
    public static final int NOT_READED = 0;
    public static final int READED = 1;
    public static final String EMAIL_ID = "info@mobjee.com";


    public static String getStatusTitle(String senderId, String senderName, String keyword) {
        return senderName + " has searched keyword : " + keyword;
    }

    public static String getStatusNotification(String senderName, String keyword) {
        return senderName + " searched " + keyword;
    }

    public static String getStatusNotificationForChat(String senderId, String senderName, String keyword, String message) {
        return senderName + " is saying : "+ message;
    }

    public static String getStatusNotificationForNotif(String senderId, String senderName, String keyword, String message) {
        return senderName + " is replying : "+ message;
    }

    public static String getStatusBody(String message) {
        return message;
    }

    public static CharSequence getTikerNotification(String senderName) {
        return senderName + " responding to your search request";
    }

    public static String sendBulkMessage() {
        return "Notification send successful.";
    }

    public static String getStatusTitleForChat(String senderName) {
        return senderName + " is saying";
    }

     public static String getStatusTitleForNotification(String senderName, String keyword) {
        return senderName + " is replying for keyword :"+keyword;
    }

    public static String getStatusBodyForChat(String message) {
        return message;
    }

}
