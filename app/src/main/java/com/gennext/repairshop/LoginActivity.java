package com.gennext.repairshop;

import android.os.Bundle;

import com.gennext.repairshop.login.LoginMobile;
import com.gennext.repairshop.login.LoginMobileVerify;


/**
 * Created by Admin on 5/22/2017.
 */

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initToolBarForTheme(this,getString(R.string.app_name));
        addFragmentWithoutBackstack(LoginMobile.newInstance(),R.id.container,"loginMobile");
    }

    public void openVerifyScreen(String mobile) {
        replaceFragment(LoginMobileVerify.newInstance(mobile),R.id.container, "loginMobileVerify");
    }
}