package com.gennext.repairshop.pannel;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.CompanyModel;
import com.gennext.repairshop.model.CompanySelectorAdapter;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCall;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.ProgressRounded;

import java.util.ArrayList;


public class CompanySelector extends BottomSheetDialogFragment implements ApiCallError.ErrorParamListener{
    private SelectListener mListener;
    private CompanySelectorAdapter adapter;
    private RecyclerView lvMain;
    //    private DBManager dbManager;
    private AssignTask assignTask;
    private ProgressRounded pBar;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public void onSelectedItem(CompanyModel item) {
        if (item != null) {
            if (mListener != null) {
                mListener.onEmployeeSelect(CompanySelector.this, item);
            }
            dismiss();
        }
    }


    public interface SelectListener {
        void onEmployeeSelect(DialogFragment dialog, CompanyModel selectedCompany);

    }


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static CompanySelector newInstance(SelectListener selectListener) {
        CompanySelector fragment = new CompanySelector();
        fragment.mListener = selectListener;
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_menu_company, container, false);
//        dbManager = DBManager.newIsntance(getActivity());
        ImageButton btnAction = (ImageButton) v.findViewById(R.id.btn_menu_action);
        final EditText etSearch = (EditText) v.findViewById(R.id.et_search);

        pBar = ProgressRounded.newInstance(getContext(), v);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().length() == 0) {
                    dismiss();
                } else {
                    etSearch.setText("");
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (adapter != null)
                    adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        lvMain = (RecyclerView) v.findViewById(R.id.rv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        executeTask();

        return v;
    }


    private void executeTask() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_COMPANIES_LIST);
    }


    @Override
    public void onErrorRetryClick(DialogFragment dialog, String[] param) {
        executeTask();
    }

    @Override
    public void onErrorCancelClick(DialogFragment dialog, String[] param) {
        dismiss();
    }


    private class AssignTask extends AsyncTask<String, Void, CompanyModel> {
        private Context context;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pBar.showProgressBar();
        }

        @Override
        protected CompanyModel doInBackground(String... urls) {
            String response = null;
//            response = ApiCall.GET(urls[0]);
            response = ApiCall.GET(urls[0]);
            return JsonParser.getCompanyList(response);
        }


        @Override
        protected void onPostExecute(CompanyModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                pBar.hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    adapter = new CompanySelectorAdapter(getActivity(), result.getList(),CompanySelector.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
//                    errorList.add(result.getOutputMsg());
//                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.slot_empty_product,
//                            R.id.tv_message, errorList);
//                    lvMain.setAdapter(adapter);
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    String[] errorSoon = {};
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), errorSoon, CompanySelector.this)
                            .show(getFragmentManager(), "apiCallError");
                }
            }
        }
    }

}
