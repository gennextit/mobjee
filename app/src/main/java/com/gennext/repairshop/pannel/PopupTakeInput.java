package com.gennext.repairshop.pannel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.gennext.repairshop.R;


public class PopupTakeInput extends DialogFragment {
    private Dialog dialog;
    private DialogListener mListener;
    private String title,buttonText;

    public interface DialogListener {
        void onOkClick(DialogFragment dialog, String message);
        void onCancelClick(DialogFragment dialog);
    }



    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PopupTakeInput newInstance(DialogListener listener) {
        return PopupTakeInput.newInstance("Enter message here.","Send",listener);
    }
    public static PopupTakeInput newInstance(String title, String buttonText,DialogListener listener) {
        PopupTakeInput fragment = new PopupTakeInput();
        fragment.mListener = listener;
        fragment.title=title;
        fragment.buttonText=buttonText;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_send_notification, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        final EditText etMessage = (EditText) v.findViewById(R.id.et_message);

        etMessage.setHint(title);

        button1.setText(buttonText);
        button2.setText("Cancel");

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onOkClick(PopupTakeInput.this, etMessage.getText().toString());
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onCancelClick(PopupTakeInput.this);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
