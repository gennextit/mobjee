package com.gennext.repairshop.model;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.user.ClientDetails;

import java.util.ArrayList;
import java.util.List;

public class ClientDetailAdapter extends RecyclerView.Adapter<ClientDetailAdapter.ReyclerViewHolder> {

    private final ClientDetails parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ClientModel> items;
    private List<Integer> progress = new ArrayList<>();

    public ClientDetailAdapter(Activity context, ArrayList<ClientModel> items, ClientDetails parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_client_detail, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ClientModel item = items.get(position);

        holder.tvName.setText(item.getName());
        holder.tvAmount.setText(item.getAmount());
        holder.tvStatus.setText(item.getStatus());


        if (item.getProgressInPercent() <= 25) {
            holder.pbProgress.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progess_drawable_red));
        } else if (item.getProgressInPercent() > 25 && item.getProgressInPercent() <= 50) {
            holder.pbProgress.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progess_drawable_yello));
        } else if (item.getProgressInPercent() > 50 && item.getProgressInPercent() <= 75) {
            holder.pbProgress.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progess_drawable_blue));
        } else if (item.getProgressInPercent() > 75 && item.getProgressInPercent() <= 100) {
            holder.pbProgress.setProgressDrawable(ContextCompat.getDrawable(context, R.drawable.progess_drawable_green));
        }
        setProgressAnimate(holder.pbProgress, 100, item.getProgressInPercent());
//        ProgressBarAnimation anim = new ProgressBarAnimation(holder.pbProgress,0,item.getProgressInPercent()*100);
//        anim.setDuration(1000);
//        holder.pbProgress.startAnimation(anim);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.onItemSelect(item);
            }
        });
    }


    private void setProgressAnimate(ProgressBar pb, int max, int progressTo) {
        pb.setMax(max * 100);
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", pb.getProgress(), progressTo * 100);
        animation.setDuration(1000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    public class ProgressBarAnimation extends Animation {
        private ProgressBar progressBar;
        private float from;
        private float to;

        public ProgressBarAnimation(ProgressBar progressBar, int from, int to) {
            super();
            this.progressBar = progressBar;
            this.from = (float) from;
            this.to = (float) to;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar pbProgress;
        private TextView tvName, tvAmount, tvStatus;

        private ReyclerViewHolder(final View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tv_clnt_name);
            tvAmount = (TextView) v.findViewById(R.id.tv_clnt_amount);
            tvStatus = (TextView) v.findViewById(R.id.tv_clnt_status);
            pbProgress = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}

