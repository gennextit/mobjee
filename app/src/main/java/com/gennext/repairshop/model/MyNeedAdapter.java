package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.R;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.MyNeed;
import com.gennext.repairshop.user.Need;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.Utility;

import java.util.ArrayList;

public class MyNeedAdapter extends RecyclerView.Adapter<MyNeedAdapter.ReyclerViewHolder> {

    private MyNeed parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<NeedModel> items;

    public MyNeedAdapter(Activity context, ArrayList<NeedModel> items, MyNeed parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_my_need, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final NeedModel item = items.get(position);

        if(item.getReadStatus() == Const.READED){
            holder.keyword.setText(Utility.generateSpannableTitle(context,item.getDetail(),"",R.color.text_readed,true));
        }else{
            holder.keyword.setText(Utility.generateSpannableTitle(context,item.getDetail(),"",R.color.text_unreaded,true));
        }

        // implement setOnClickListener event on item view.
        holder.time.setText(converteTimestamp(item.getTimestamp()));

        holder.remove.setVisibility(View.VISIBLE);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.revoveMyNeed(item,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView keyword,time;
        private ImageView remove;

        public ReyclerViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            keyword = (TextView) itemView.findViewById(R.id.tv_keyword);
            time = (TextView) itemView.findViewById(R.id.tv_keyword_time);
            remove = (ImageView) itemView.findViewById(R.id.iv_keyword);
        }
    }

    private CharSequence converteTimestamp(long mileSegundos){
        return DateUtils.getRelativeTimeSpanString(mileSegundos, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }
}

