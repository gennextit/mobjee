package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.R;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.user.chat.ChatList;

import java.util.ArrayList;
import java.util.List;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ReyclerViewHolder> {

    private final ChatList parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private List<ChatListModel> originalData;
    private List<ChatListModel> items;//filterData
    private ItemFilter mFilter = new ItemFilter();

    public FriendListAdapter(Activity context, ArrayList<ChatListModel> items, ChatList parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.originalData = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_friends, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ChatListModel item = items.get(position);

        // set the data in items
        holder.name.setText(item.getFullName());
        holder.address.setText(item.getAddress());
        if(item.getCounter()==0){
            holder.counter.setText("");
            holder.counter.setVisibility(View.GONE);
        }else {
            holder.counter.setText(String.valueOf(item.getCounter()));
            holder.counter.setVisibility(View.VISIBLE);
        }
        // implement setOnClickListener event on item view.
        Glide.with(context)
                .load(item.getImageUrl()).centerCrop()
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .override(80,80).into(holder.profile);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.onItemSelect(item, getItemPositionInList(item));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                parentRef.onLongItemClicked(item, getItemPositionInList(item));
                return false;
            }
        });
    }

    private int getItemPositionInList(ChatListModel item) {
        for (int i = 0; i<originalData.size(); i++){
            if(item.getReceiverId().equalsIgnoreCase(originalData.get(i).getReceiverId())){
                return i;
            }
        }
        return 0;
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,counter;
        ImageView profile;

        public ReyclerViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            counter = (TextView) itemView.findViewById(R.id.tv_counter);
            name = (TextView) itemView.findViewById(R.id.tv_keyword);
            address = (TextView) itemView.findViewById(R.id.tv_address);
            profile = (ImageView) itemView.findViewById(R.id.iv_profile);
        }
    }


    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ChatListModel> list = originalData;

            int count = list.size();
            final ArrayList<ChatListModel> nlist = new ArrayList<>(count);
            String filterableText;

            if (!filterString.equals("")) {
                for (ChatListModel model : list) {
                    if (!filterString.equals("")) {
                        filterableText = model.getFullName();
                        if (filterableText != null && filterableText.toLowerCase().contains(filterString)) {
                            nlist.add(model);
                        }
                    } else {
                        nlist.add(model);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            } else {
                results.values = originalData;
                results.count = originalData.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<ChatListModel>) results.values;
            notifyDataSetChanged();
        }

    }

}

