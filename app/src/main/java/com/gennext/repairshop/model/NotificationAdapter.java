package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.notification.Notification;
import com.gennext.repairshop.util.Utility;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ReyclerViewHolder> {

    private Notification parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<NotificationModel> items;

    public NotificationAdapter(Activity context, ArrayList<NotificationModel> items, Notification parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_notification, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final NotificationModel item = items.get(position);

        // set the data in items
//        if(item.getKeyword().equalsIgnoreCase(Const.REPLIED_CHAT)){
//            holder.name.setText(Const.getStatusNotificationForChat(item.getSenderId(), item.getSenderName(), item.getKeyword(), item.getMessage()));
//        }else if(item.getKeyword().equalsIgnoreCase(Const.REPLIED_NOTIFICATION)){
//            holder.name.setText(Const.getStatusNotificationForNotif(item.getSenderId(), item.getSenderName(), item.getKeyword(), item.getMessage()));
//        }else{
//        }

        String title = Const.getStatusNotification(item.getSenderName(), item.getKeyword());
        holder.title.setText(title);
        holder.name.setText(item.getMessage());

        if(item.getReadStatus() == Const.READED){
//            holder.slot.setBackgroundResource(R.drawable.bg_menu_fab);
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.text_readed));
        }else{
            holder.name.setTextColor(ContextCompat.getColor(context,R.color.text_unreaded));
//            holder.slot.setBackgroundResource(R.drawable.bg_menu_fab_lite);
        }
        // implement setOnClickListener event on item view.
        holder.time.setText(converteTimestamp(item.getTimestamp()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.onItemClick(item,position);
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.revoveNotification(item,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout slot;
        TextView name,time,title;
        ImageView remove;

        public ReyclerViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            slot = (LinearLayout) itemView.findViewById(R.id.layout_slot);
            title = (TextView) itemView.findViewById(R.id.tv_keyword_title);
            name = (TextView) itemView.findViewById(R.id.tv_keyword);
            time = (TextView) itemView.findViewById(R.id.tv_keyword_time);
            remove = (ImageView) itemView.findViewById(R.id.iv_keyword);
        }
    }

    private CharSequence converteTimestamp(long mileSegundos){
        return DateUtils.getRelativeTimeSpanString(mileSegundos, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }
}

