package com.gennext.repairshop.model;

import java.util.ArrayList;

public class ClientModel {

    private String name;
    private String amount;
    private String status;
    private int progressInPercent;

    private String output;
    private String outputMsg;
    private ArrayList<ClientModel> list;

    public ClientModel() {
    }

    public ClientModel(String name, String amount, String status, int progressInPercent) {
        this.name = name;
        this.amount = amount;
        this.status = status;
        this.progressInPercent = progressInPercent;
    }

    public ClientModel(String name, String amount, String status, int progressInPercent, ArrayList<ClientModel> list) {
        this.name = name;
        this.amount = amount;
        this.status = status;
        this.progressInPercent = progressInPercent;
        this.list = list;
    }

    public ArrayList<ClientModel> getList() {
        return list;
    }

    public void setList(ArrayList<ClientModel> list) {
        this.list = list;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getProgressInPercent() {
        return progressInPercent;
    }

    public void setProgressInPercent(int progressInPercent) {
        this.progressInPercent = progressInPercent;
    }
}
