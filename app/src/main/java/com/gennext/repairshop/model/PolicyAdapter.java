package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.R;

import java.util.ArrayList;

public class PolicyAdapter extends RecyclerView.Adapter<PolicyAdapter.ReyclerViewHolder> {

    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<PolicyModel> items;

    public PolicyAdapter(Activity context, ArrayList<PolicyModel> items) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_policy, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final PolicyModel item = items.get(position);

        holder.tvName.setText(item.getProposerName());
        holder.tvPeriod.setText(item.getPolicyPeriod());
        holder.tvBenefit.setText(item.getCumulativeBenifit());
        holder.tvNumber.setText(item.getPolicyNumber());
        holder.tvSumInsured.setText(item.getSumInsured());

        holder.llSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               shopListViewActivity.addFragment(ShopDetail.newInstance(context,item,mCurrentLocation),"shopDetail");
//                parentRef.startColorPicker(position,item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout llSlot;
        private TextView tvName,tvPeriod, tvBenefit, tvNumber ,tvSumInsured;

        private ReyclerViewHolder(final View v) {
            super(v);
            llSlot = (LinearLayout) v.findViewById(R.id.layoutSlot);
            tvName = (TextView) v.findViewById(R.id.tv_field_1);
            tvPeriod = (TextView) v.findViewById(R.id.tv_field_2);
            tvBenefit = (TextView) v.findViewById(R.id.tv_field_3);
            tvNumber = (TextView) v.findViewById(R.id.tv_field_4);
            tvSumInsured = (TextView) v.findViewById(R.id.tv_field_5);
        }
    }
}

