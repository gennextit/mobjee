package com.gennext.repairshop.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ChatListModel implements Parcelable {

    private String senderId;
    private String receiverId;
    private String firebaseId;
    private String fullName;
    private String imageUrl;
    private String mobileNumber;
    private String address;
    private String latLng;
    private String tilestamp;
    private String gcmId;
    private String status;
    private int counter;

    private String output;
    private String outputMsg;
    private ArrayList<ChatListModel> list;


    public ChatListModel() {
    }

    public ChatListModel(String senderId, String receiverId, String firebaseId, String fullName, String imageUrl, String mobileNumber, String latLng, String address, String gcmId, int counter, String status) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.firebaseId = firebaseId;
        this.fullName = fullName;
        this.imageUrl = imageUrl;
        this.mobileNumber = mobileNumber;
        this.latLng = latLng;
        this.address = address;
        this.gcmId = gcmId;
        this.counter = counter;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getFirebaseId() {
        return firebaseId;
    }

    public void setFirebaseId(String firebaseId) {
        this.firebaseId = firebaseId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLatLng() {
        return latLng;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }

    public String getTilestamp() {
        return tilestamp;
    }

    public void setTilestamp(String tilestamp) {
        this.tilestamp = tilestamp;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<ChatListModel> getList() {
        return list;
    }

    public void setList(ArrayList<ChatListModel> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.senderId);
        dest.writeString(this.receiverId);
        dest.writeString(this.firebaseId);
        dest.writeString(this.fullName);
        dest.writeString(this.imageUrl);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.latLng);
        dest.writeString(this.tilestamp);
        dest.writeString(this.address);
        dest.writeString(this.gcmId);
        dest.writeString(this.output);
        dest.writeString(this.status);
        dest.writeString(this.outputMsg);
        dest.writeInt(this.counter);
        dest.writeList(this.list);
    }

    protected ChatListModel(Parcel in) {
        this.senderId = in.readString();
        this.receiverId = in.readString();
        this.firebaseId = in.readString();
        this.fullName = in.readString();
        this.imageUrl = in.readString();
        this.mobileNumber = in.readString();
        this.address = in.readString();
        this.latLng = in.readString();
        this.tilestamp = in.readString();
        this.gcmId = in.readString();
        this.status = in.readString();
        this.output = in.readString();
        this.outputMsg = in.readString();
        this.counter = in.readInt();
        this.list = new ArrayList<ChatListModel>();
        in.readList(this.list, ChatListModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<ChatListModel> CREATOR = new Parcelable.Creator<ChatListModel>() {
        @Override
        public ChatListModel createFromParcel(Parcel source) {
            return new ChatListModel(source);
        }

        @Override
        public ChatListModel[] newArray(int size) {
            return new ChatListModel[size];
        }
    };
}
