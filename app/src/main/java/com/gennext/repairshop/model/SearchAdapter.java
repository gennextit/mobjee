package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.R;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.search.SearchBarView;
import com.gennext.repairshop.user.chat.ChatList;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ReyclerViewHolder> {

    private final SearchBarView parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<ProfileModel> items;

    public SearchAdapter(Activity context, ArrayList<ProfileModel> items, SearchBarView parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_search, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final ProfileModel item = items.get(position);

        // set the data in items
        holder.name.setText(item.getFullName());
        holder.distance.setText(item.getDistance()+" Km");
        holder.mobile.setText(item.getMobileNumber());
        holder.address.setText(item.getAddress());

        Glide.with(context)
                .load(item.getImageUrl()).centerCrop()
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .override(50,50).into(holder.profile);

        // implement setOnClickListener event on item view.
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.onItemSelect(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobile, address, distance;
        ImageView profile;

        public ReyclerViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            profile = (ImageView) itemView.findViewById(R.id.iv_profile);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            distance = (TextView) itemView.findViewById(R.id.tv_distance);
            mobile = (TextView) itemView.findViewById(R.id.tv_mobile);
            address = (TextView) itemView.findViewById(R.id.tv_address);
        }
    }
}

