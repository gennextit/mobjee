package com.gennext.repairshop.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.R;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.Need;
import com.gennext.repairshop.user.notification.Notification;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.Utility;

import java.util.ArrayList;

public class NeedAdapter extends RecyclerView.Adapter<NeedAdapter.ReyclerViewHolder> {

    private final String user;
    private Need parentRef;
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<NeedModel> items;

    public NeedAdapter(Activity context, ArrayList<NeedModel> items, Need parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.user = AppUser.getUserId(context);
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.slot_user_need, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final NeedModel item = items.get(position);

        if(item.getReadStatus() == Const.READED){
            holder.keyword.setText(Utility.generateSpannableTitle(context,item.getDetail(),"",R.color.text_readed,true));
        }else{
            holder.keyword.setText(Utility.generateSpannableTitle(context,item.getDetail(),"",R.color.text_unreaded,true));
        }
        holder.person.setText(item.getName());

        Glide.with(context)
                .load(item.getImage()).centerCrop()
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .override(60,60).into(holder.image);

        // implement setOnClickListener event on item view.
        holder.time.setText(converteTimestamp(item.getTimestamp()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.onItemClick(item,position);
            }
        });
//        if(item.getUserId().equalsIgnoreCase(user)){
//            holder.remove.setVisibility(View.VISIBLE);
//        }else{
//        }

        holder.remove.setVisibility(View.GONE);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parentRef.revoveNotification(item,position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView keyword,time,person;
        private ImageView remove;

        public ReyclerViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            image = (ImageView) itemView.findViewById(R.id.iv_profile);
            keyword = (TextView) itemView.findViewById(R.id.tv_keyword);
            time = (TextView) itemView.findViewById(R.id.tv_keyword_time);
            person = (TextView) itemView.findViewById(R.id.tv_keyword_name);
            remove = (ImageView) itemView.findViewById(R.id.iv_keyword);
        }
    }

    private CharSequence converteTimestamp(long mileSegundos){
        return DateUtils.getRelativeTimeSpanString(mileSegundos, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }
}

