package com.gennext.repairshop.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.user.profile.MyKeywords;

import java.util.ArrayList;

public class KeywordAdapter extends  RecyclerView.Adapter<KeywordAdapter.MyViewHolder> {
    private MyKeywords parentRef;
    ArrayList<String> personNames;
    Context context;

    public KeywordAdapter(Context context, ArrayList<String> personNames, MyKeywords parentRef) {
        this.context = context;
        this.personNames = personNames;
        this.parentRef = parentRef;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.slot_keyword, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items
        holder.name.setText(personNames.get(position));
        // implement setOnClickListener event on item view.
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                personNames.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, personNames.size());
                parentRef.mItemAddedInList = true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView name;
        ImageView remove;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            name = (TextView) itemView.findViewById(R.id.tv_keyword);
            remove = (ImageView) itemView.findViewById(R.id.iv_keyword);
        }
    }
}