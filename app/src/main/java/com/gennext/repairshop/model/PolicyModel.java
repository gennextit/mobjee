package com.gennext.repairshop.model;

import java.util.ArrayList;

public class PolicyModel {


    private String proposerName;
    private String policyPeriod;
    private String cumulativeBenifit;
    private String policyNumber;
    private String sumInsured;
    private String output;
    private String outputMsg;
    private ArrayList<PolicyModel> list;


    public PolicyModel() {    }

    public PolicyModel(String proposerName, String policyPeriod, String cumulativeBenifit, String policyNumber, String sumInsured) {
        this.proposerName = proposerName;
        this.policyPeriod = policyPeriod;
        this.cumulativeBenifit = cumulativeBenifit;
        this.policyNumber = policyNumber;
        this.sumInsured = sumInsured;
    }

    public String getProposerName() {
        return proposerName;
    }

    public void setProposerName(String proposerName) {
        this.proposerName = proposerName;
    }

    public String getPolicyPeriod() {
        return policyPeriod;
    }

    public void setPolicyPeriod(String policyPeriod) {
        this.policyPeriod = policyPeriod;
    }

    public String getCumulativeBenifit() {
        return cumulativeBenifit;
    }

    public void setCumulativeBenifit(String cumulativeBenifit) {
        this.cumulativeBenifit = cumulativeBenifit;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }

    public ArrayList<PolicyModel> getList() {
        return list;
    }

    public void setList(ArrayList<PolicyModel> list) {
        this.list = list;
    }
}
