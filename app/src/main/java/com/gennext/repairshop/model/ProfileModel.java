package com.gennext.repairshop.model;

import java.util.ArrayList;

public class ProfileModel {

    private String candidateId;
    private String fullName;
    private String address;
    private String pinCode;
    private String imageUrl;
    private String mobileNumber;
    private String keywords;
    private String latLng;
    private String distance;
    private String radius;
    private String gcmId;

    private String output;
    private String outputMsg;
    private ArrayList<ProfileModel> list;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public ProfileModel() {
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public ProfileModel(String candidateId, String fullName, String address, String pinCode, String imageUrl, String mobileNumber, String keywords, String latLng) {
        this.candidateId = candidateId;
        this.fullName = fullName;
        this.address = address;
        this.pinCode = pinCode;
        this.imageUrl = imageUrl;
        this.mobileNumber = mobileNumber;
        this.keywords = keywords;
        this.latLng = latLng;
    }

    public ArrayList<ProfileModel> getList() {
        return list;
    }

    public void setList(ArrayList<ProfileModel> list) {
        this.list = list;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(String candidateId) {
        this.candidateId = candidateId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLatLng() {
        return latLng;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
