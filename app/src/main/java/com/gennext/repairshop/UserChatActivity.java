package com.gennext.repairshop;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.chat.ChatFirebaseAdapter;
import com.gennext.repairshop.model.chat.ChatModel;
import com.gennext.repairshop.model.chat.ClickListenerChatFirebase;
import com.gennext.repairshop.model.chat.FileModel;
import com.gennext.repairshop.model.chat.MapModel;
import com.gennext.repairshop.model.chat.UserModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.chat.ChatNotify;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.FileUtils;
import com.gennext.repairshop.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import io.fabric.sdk.android.Fabric;

public class UserChatActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, ClickListenerChatFirebase {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;

    static final String TAG = MainActivity.class.getSimpleName();
    static final String CHAT_REFERENCE = "shopchat";

    private DatabaseReference mFirebaseDatabaseReference;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    //CLass Model
    private UserModel userModel;

    //Views UI
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView btSendMessage, btLocation;
    private EditText edMessage;
    private View contentRoot;

    //File
    private File filePathImageCamera;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private ChatListModel chatModel;
    private String mSenderId, mReceiverId, mFirebaseId;
    //    private ImageView ivFab;
    Boolean flagFab = true;
    private ImageView btCamera;
    private LinearLayout llProgress;
    private ProgressBar pbProgressMain;
    private Toolbar toolbar;
    private Uri filePathImageCameraUri;
    private ChatNotify chatNotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat);
        toolbar = initToolBar(" ");
        if (!Util.checkInternetConnection(this)) {
            Util.initToast(this, "Internet connection not available");
            finish();
        } else {
            bindViews();
            updateUser(getIntent().getExtras());
        }
    }

    private void updateUser(Bundle extras) {
        if (extras != null) {
            chatModel = extras.getParcelable("user");
            String message = extras.getString("message");
            chatNotify = ChatNotify.newInstance(message,chatModel);
            addFragmentWithoutBackstack(chatNotify,R.id.container_chat,"chatNotify");
            if (chatModel != null) {
                toolbar.setTitle(chatModel.getFullName());
                mSenderId = chatModel.getSenderId();
                mReceiverId = chatModel.getReceiverId();
                mFirebaseId = chatModel.getFirebaseId();
                String name = AppUser.getName(UserChatActivity.this);
                String image = AppUser.getImage(UserChatActivity.this);
                userModel = new UserModel(name, image, chatModel.getSenderId());
                loadMessagensFirebase();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
//                    sendFileFirebase(storageRef, selectedImageUri);
                    startCropImageActivity(selectedImageUri);
                } else {
                    showToast("Invalid Image Path");
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                String imageName = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
//                filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName + "camera.jpg");
//                filePathImageCameraUri = FileProvider.getUriForFile(ProfileActivity.this,
//                        BuildConfig.APPLICATION_ID + ".provider",
//                        filePathImageCamera);
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                filePathImageCameraUri = FileUtils.getImageUri(UserChatActivity.this,imageBitmap,imageName);

                if (filePathImageCameraUri != null) {
//                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() + "_camera");
//                    sendFileFirebase(imageCameraRef, filePathImageCamera);
                    startCropImageActivity(filePathImageCameraUri);
                } else {
                    showToast("Invalid Image Path");
                }
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                sendFileFirebase(storageRef, result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: please select a valid Image" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    MapModel mapModel = new MapModel(latLng.latitude + "", latLng.longitude + "");
                    ChatModel chatModel = new ChatModel(userModel, Calendar.getInstance().getTime().getTime() + "", mapModel);
                    mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(mFirebaseId).push().setValue(chatModel);
                    sendChatNotification(Const.SEND_LOCATION);
                } else {
                    showToast("Invalid Location Path");
                }
            }
        }

    }


    private void startCropImageActivity(Uri imageUri) {
        if (imageUri != null) {
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setFixAspectRatio(false)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        } else {
            Toast.makeText(UserChatActivity.this, "Please Reselect image ", Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_chat, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//            case R.id.sendPhoto:
//                verifyStoragePermissions();
////                photoCameraIntent();
//                break;
//            case R.id.sendPhotoGallery:
//                photoGalleryIntent();
//                break;
//            case R.id.sendLocation:
//                locationPlacesIntent();
//                break;
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Util.initToast(this, "Google Play Services error.");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addBtn:
                String message = edMessage.getText().toString().trim();
                if (!message.equals("")) {
                    sendMessageFirebase();
                } else {
                    photoGalleryIntent();
                }
                break;
            case R.id.button_camera:
                verifyStoragePermissions();
                break;

            case R.id.button_location:
                locationPlacesIntent();
                break;
        }
    }

    @Override
    public void clickImageChat(View view, int position, String nameUser, String urlPhotoUser, String urlPhotoClick) {
        Intent intent = new Intent(this, FullScreenImageActivity.class);
        intent.putExtra("nameUser", nameUser);
        intent.putExtra("urlPhotoUser", urlPhotoUser);
        intent.putExtra("urlPhotoClick", urlPhotoClick);
        startActivity(intent);
    }

    @Override
    public void clickImageMapChat(View view, int position, String latitude, String longitude) {
        String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude, longitude, latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
//        String filename = (new File(file.getPath())).getName();
        String filename = MimeTypeMap.getFileExtensionFromUrl(file.toString());
        if (storageReference != null) {
//            if (filename.toLowerCase().contains("jpg") || filename.toLowerCase().contains("jpeg") || filename.toLowerCase().contains("png")) {
            llProgress.setVisibility(View.VISIBLE);
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    llProgress.setVisibility(View.GONE);
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
//                        String downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            llProgress.setVisibility(View.GONE);
                            String photoStringLink = uri.toString();
                            FileModel fileModel = new FileModel("img", photoStringLink, name, "");
                            ChatModel chatModel = new ChatModel(userModel, "", Calendar.getInstance().getTime().getTime() + "", fileModel);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(mFirebaseId).push().setValue(chatModel);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            llProgress.setVisibility(View.GONE);
                        }
                    });

                }
            });

        } else {
            //IS NULL
        }

        sendChatNotification(Const.SEND_PHOTO);
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {
            llProgress.setVisibility(View.VISIBLE);
            Uri photoURI = FileProvider.getUriForFile(UserChatActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    llProgress.setVisibility(View.GONE);
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
//                    String downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            llProgress.setVisibility(View.GONE);
                            String photoStringLink = uri.toString();
                            FileModel fileModel = new FileModel("img", photoStringLink, file.getName(), file.length() + "");
                            ChatModel chatModel = new ChatModel(userModel, "", Calendar.getInstance().getTime().getTime() + "", fileModel);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(mFirebaseId).push().setValue(chatModel);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            llProgress.setVisibility(View.GONE);
                        }
                    });

                }
            });
        } else {
            //IS NULL
        }

    }

    /**
     * Obter local do usuario
     */
    private void locationPlacesIntent() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent() {
//        String imageName = DateFormat.format("camera-yyyy-MM-dd_hhmmss", new Date()).toString();
//        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName + "camera.jpg");
//        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        filePathImageCameraUri = FileProvider.getUriForFile(UserChatActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                filePathImageCamera);
//        it.putExtra(MediaStore.EXTRA_OUTPUT, filePathImageCameraUri);
//        startActivityForResult(it, IMAGE_CAMERA_REQUEST);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
        }
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    /**
     * Enviar msg de texto simples para chat
     */
    private void sendMessageFirebase() {
        ChatModel model = new ChatModel(userModel, edMessage.getText().toString(), Calendar.getInstance().getTime().getTime() + "", null);
        mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(mFirebaseId).push().setValue(model);

        sendChatNotification(edMessage.getText().toString());
        edMessage.setText(null);
    }

    private void sendChatNotification(String message) {
        if(chatNotify!=null)
            chatNotify.sendNotification(message);
    }

    /**
     * Ler collections chatmodel Firebase
     */
    private void loadMessagensFirebase() {
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        final ChatFirebaseAdapter firebaseAdapter = new ChatFirebaseAdapter(mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(mFirebaseId), userModel.getId(), this);
        firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
//                pbProgressMain.setVisibility(View.GONE);
                int friendlyMessageCount = firebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    rvListMessage.scrollToPosition(positionStart);
                }
            }
        });
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.setAdapter(firebaseAdapter);
        animateButtonAndRevert();
    }

    private void animateButtonAndRevert() {
//        Handler handler = new Handler();
//        Runnable runnableRevert = new Runnable() {
//            @Override
//            public void run() {
//                if (pbProgressMain != null) {
//                    pbProgressMain.setVisibility(View.GONE);
//                }
//            }
//        };
//        handler.postDelayed(runnableRevert, 4000);
    }


    /**
     * Vincular views com Java API
     */
    private void bindViews() {
        contentRoot = findViewById(R.id.contentRoot);
        edMessage = (EditText) findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView) findViewById(R.id.buttonMessage);
        RelativeLayout rlSend = (RelativeLayout) findViewById(R.id.addBtn);
        btLocation = (ImageView) findViewById(R.id.button_location);
        btCamera = (ImageView) findViewById(R.id.button_camera);
        rlSend.setOnClickListener(this);
        btLocation.setOnClickListener(this);
        btCamera.setOnClickListener(this);
        rvListMessage = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        llProgress = (LinearLayout) findViewById(R.id.ll_progress);
        pbProgressMain = (ProgressBar) findViewById(R.id.progressBar2);
//        pbProgressMain.setVisibility(View.VISIBLE);

        edMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Bitmap img = BitmapFactory.decodeResource(getResources(), R.drawable.ic_send_white_24dp);
                Bitmap img1 = BitmapFactory.decodeResource(getResources(), R.drawable.ic_attachment_black_48dp);


                if (s.toString().trim().length() != 0 && flagFab) {
                    ImageViewAnimatedChange(UserChatActivity.this, btSendMessage, img);
                    flagFab = false;
//                    btLocation.setVisibility(View.GONE);
                    AppAnimation.hideViewFadeOut(UserChatActivity.this,btLocation);
                } else if (s.toString().trim().length() == 0) {
                    ImageViewAnimatedChange(UserChatActivity.this, btSendMessage, img1);
                    flagFab = true;
//                    btLocation.setVisibility(View.VISIBLE);
                    AppAnimation.showViewFadeIn(UserChatActivity.this,btLocation);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(UserChatActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    UserChatActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            // we already have permission, lets go ahead and call camera intent
            photoCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    photoCameraIntent();
                }
                break;
        }
    }

    public void ImageViewAnimatedChange(Context c, final ImageView v, final Bitmap new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation(c, R.anim.zoom_out);
        final Animation anim_in = AnimationUtils.loadAnimation(c, R.anim.zoom_in);
        anim_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setImageBitmap(new_image);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }
}
