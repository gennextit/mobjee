package com.gennext.repairshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.gennext.repairshop.fcm.MyFirebaseMessagingService;
import com.gennext.repairshop.util.AppUser;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Abhijit-PC on 17-Mar-17.
 */

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MyFirebaseMessagingService.chattingUserId = "";
        if (!AppUser.getUserId(this).equals("")) {
            if (!AppUser.getName(this).equals("")) {
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(SplashScreen.this, ProfileActivity.class));
                finish();
            }
        } else {
            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
            finish();
        }
    }
}
