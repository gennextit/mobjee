package com.gennext.repairshop.search;

/**
 * Created by Admin on 5/22/2017.
 */

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.util.DetailsTransition;

/**
 * Usage
 //    private void addSearchTag(Bundle savedInstanceState) {
//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.container_bar, ChatSearchBar.newInstance(ClassName.this),"searchBar")
//                    .commit();
//        }
//    }
 *
 */

public class ChatSearchBar extends CompactFragment {



    private ImageView ivSearch;
    private RelativeLayout rlLayout;
    private ChatSearch listener;

    public interface ChatSearch{
        void onTextChanged(CharSequence charSequence);
    }

    public static Fragment newInstance(ChatSearch listener) {
        ChatSearchBar fragment=new ChatSearchBar();
        fragment.listener=listener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.search_bar_chat, container, false);
        InitUI(v);
        return v;
    }
    private void InitUI(View v) {
        rlLayout = (RelativeLayout) v.findViewById(R.id.rl_layout);
        ivSearch = (ImageView) v.findViewById(R.id.imageView3);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                ChatSearchBarView fragment = ChatSearchBarView.newInstance(listener);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    showTargetFragmentLollipop(fragment, rlLayout);
                    return;
                }
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container_bar_chat, fragment,"chatSearchBarView")
                        .addToBackStack("chatSearchBarView")
                        .commit();
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showTargetFragmentLollipop(ChatSearchBarView fragment, View forImageView) {
        fragment.setSharedElementEnterTransition(new DetailsTransition());
        fragment.setEnterTransition(new Fade());
        setExitTransition(new Fade());
        fragment.setSharedElementReturnTransition(new DetailsTransition());

        String transitionName = ViewCompat.getTransitionName(forImageView);
        getFragmentManager()
                .beginTransaction()
                .addSharedElement(forImageView, transitionName)
                .replace(R.id.container_bar_chat, fragment,"chatSearchBarView")
                .addToBackStack("chatSearchBarView")
                .commit();
    }

}
