package com.gennext.repairshop.search;

/**
 * Created by Admin on 3/15/2018.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.model.ProfileModel;
import com.gennext.repairshop.model.SearchAdapter;
import com.gennext.repairshop.pannel.PopupTakeInput;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.notification.NotificationDetail;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.JsonCreator;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class SearchBarView extends CompactFragment {


    private EditText etSearch;
    private SearchBar.Search mListener;
    private ApiRequest taskReq;
    private RecyclerView lvMain;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<ProfileModel> cList;
    private SearchAdapter adapter;
    private Button btnSendNotification;
    private AlertDialog pDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    public static SearchBarView newInstance(SearchBar.Search listener) {
        SearchBarView fragment = new SearchBarView();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mListener = listener;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.search_bar_view, container, false);
        initUi(v);

        return v;
    }


    private void initUi(View v) {
        final ImageButton btnAction = (ImageButton) v.findViewById(R.id.ib_menu_action);
        btnSendNotification = (Button) v.findViewById(R.id.btn_send_notification);
        etSearch = (EditText) v.findViewById(R.id.et_search);
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateUi();
            }
        });


//        etSearch.setImeActionLabel("Search", EditorInfo.IME_ACTION_SEARCH);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                boolean handled = false;
                if (actionId == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE|| actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Handle pressing "Enter" key here
                    btnAction.performClick();
                    handled = true;
                }
                return handled;
            }
        });
        btnSendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupTakeInput.newInstance(new PopupTakeInput.DialogListener() {
                    @Override
                    public void onOkClick(DialogFragment dialog, String message) {
                        sendNotification(message);
                    }

                    @Override
                    public void onCancelClick(DialogFragment dialog) {

                    }
                }).show(getFragmentManager(),"popupTakeInput");
            }
        });

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearch.getText().length() == 0) {
                    hideKeybord(getActivity());
                    getFragmentManager().popBackStack();
                } else {
                    updateUi();
                    hideKeybord(getActivity());
                }
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mListener.onTextChanged(charSequence);
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        showSoftKeyboard(etSearch,getContext());
    }


    private void hideProgressBar() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    private void updateUi() {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                mSwipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                });
            }

            @Override
            public void onResponse(String response) {
                hideProgressBar();
                 ProfileModel result = JsonParser.loadSearchResult(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    cList = result.getList();
                    Collections.sort(cList, new Comparator<ProfileModel>() {
                        @Override
                        public int compare(ProfileModel item, ProfileModel item2) {
                            Double date = Double.parseDouble(item.getDistance());
                            Double date2 = Double.parseDouble(item2.getDistance());
                            return date.compareTo(date2);
                        }
                    });

                    adapter = new SearchAdapter(getActivity(), cList, SearchBarView.this);
                    lvMain.setAdapter(adapter);
                    btnSendNotification.setVisibility(View.VISIBLE);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    setErrorAdapter(lvMain, result.getOutputMsg());
                    btnSendNotification.setVisibility(View.GONE);
                } else {
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                    btnSendNotification.setVisibility(View.GONE);
                }
            }

            @Override
            public void onRetryClick() {
                hideProgressBar();
                updateUi();
            }
        });
        String userId = AppUser.getUserId(getContext());
        String latitude = AppUser.getLatitude(getContext());
        String longitude = AppUser.getLongitude(getContext());
        String keyword = etSearch.getText().toString();
        String radius = "100000";
        taskReq.execute(ApiRequest.POST, AppSettings.FIND_ACC_OWNER, RequestBuilder.findAccOwner(userId, keyword, latitude, longitude, radius));

    }

    private void sendNotification(final String message) {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                pDialog = PopupProgress.newInstance(getContext()).show();
            }

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Model result = JsonParser.defaultParser(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    PopupAlert.newInstance(getString(R.string.alert), Const.sendBulkMessage(),result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                pDialog.dismiss();
                sendNotification(message);
            }
        });
        String senderId = AppUser.getUserId(getContext());
        String keyword = etSearch.getText().toString();
        String receiverIds = JsonCreator.generateReceiverIds(cList);
        taskReq.execute(ApiRequest.POST, AppSettings.SEND_NOTIFICATION, RequestBuilder.sendNotification(senderId, keyword, message, receiverIds));

    }


    public void resetSearchBox() {
        etSearch.setText("");
    }

    public void onItemSelect(ProfileModel item) {
        NotificationModel model = parseDetail(item);
        addFragment(NotificationDetail.newinstance(model),"notificationDetail");
    }

    private NotificationModel parseDetail(ProfileModel model) {
        NotificationModel item = new NotificationModel();
        item.setNotificationId("");
        item.setTimestamp(0);
        item.setSenderId(model.getCandidateId());
        item.setSenderName(model.getFullName());
        item.setSenderAddress(model.getAddress());
        item.setSenderMobile(model.getMobileNumber());
        item.setSenderImage(model.getImageUrl());
        item.setSenderLocation(model.getLatLng());
        item.setKeyword(model.getKeywords());
        item.setGcmId(model.getGcmId());
        item.setMessage("");
        return item;
    }
}
