package com.gennext.repairshop.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.CircularsAdapter;
import com.gennext.repairshop.model.CircularsModel;
import com.gennext.repairshop.model.ClientModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCall;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.RequestBuilder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class ClientProjectUpdate extends CompactFragment {

    private ClientModel mClientDetail;

    public static Fragment newInstance(ClientModel item) {
        ClientProjectUpdate fragment=new ClientProjectUpdate();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mClientDetail=item;
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_project_update,container,false);
        initToolBar(getActivity(),v,mClientDetail.getName());
        initUi(v);
        return v;
    }

    private void initUi(View v) {

    }


}
