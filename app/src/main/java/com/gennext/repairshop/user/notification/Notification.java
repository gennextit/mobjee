package com.gennext.repairshop.user.notification;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.UserChatActivity;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupDialog;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.CircularsAdapter;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.NotificationAdapter;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCall;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.DBHelper;
import com.gennext.repairshop.util.DBManager;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.RequestBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Admin on 7/4/2017.
 */

public class Notification extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<NotificationModel> cList;
    private NotificationAdapter adapter;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ApiRequest taskReq;
    private AlertDialog pDialog;
    private LinearLayout llEmptyDetail;
    private DBHelper dbHelper;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbHelper!=null){
            dbHelper.closeDB();
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }
    public static Notification newInstance() {
        Notification fragment=new Notification();
//        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_notification,container,false);
        dbHelper = new DBHelper(getContext());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        llEmptyDetail = includeEmptyListView(v,"No new notifications!",getString(R.string.notification_detail_message),R.drawable.ic_nav_notification2);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });

        executeTask();
    }



    public void refreshList() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_NOTIFICATION);
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_NOTIFICATION);
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void revoveNotification(final NotificationModel item, final int position) {
        PopupDialog.newInstance("Alert", "Are you sure to delete this notification?", new PopupDialog.DialogListener() {
            @Override
            public void onOkClick(DialogFragment dialog) {
                removeNotification(item,position);
            }

            @Override
            public void onCancelClick(DialogFragment dialog) {

            }
        }).show(getFragmentManager(),"popupDialog");
    }

    public void onItemClick(NotificationModel item, int position) {
        dbHelper.insertNotificationCounter(AppUser.getUserId(getContext()),item.getNotificationId(),Const.READED);
        item.setReadStatus(Const.READED);
        cList.set(position,item);
        adapter.notifyDataSetChanged();
        addFragment(NotificationDetail.newinstance(item),"notificationDetail");
    }

    private void removeNotification(final NotificationModel item, final int position) {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                pDialog = PopupProgress.newInstance(getContext()).show();
            }

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Model result = JsonParser.defaultParser(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    removeItemFromList(item,position);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                pDialog.dismiss();
                removeNotification(item,position);
            }
        });
        String receiverId = AppUser.getUserId(getContext());
        String notificationId = item.getNotificationId();
        taskReq.execute(ApiRequest.POST, AppSettings.DELETE_NOTIFICATION, RequestBuilder.deleteNotification(receiverId, notificationId));
    }

    private void removeItemFromList(NotificationModel item, int position) {
        dbHelper.deleteNotificationCounter(AppUser.getUserId(getContext()),item.getNotificationId());
        cList.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, cList.size());
    }


    private class AssignTask extends AsyncTask<String, Void, NotificationModel> {
        private Context context;
        HashMap<String, String> data;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected NotificationModel doInBackground(String... urls) {
            String response = null;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.receiverId(userId));
            NotificationModel result = JsonParser.parseNotification(response);
            NotificationModel dbList = dbHelper.getNotificationList(AppUser.getUserId(context));
            if(result.getOutput().equals(Const.SUCCESS) && dbList.getOutput().equals(Const.SUCCESS)){
                return JsonParser.mergeList(result.getList(),dbList.getList());
            }else {
                return result;
            }
        }


        @Override
        protected void onPostExecute(NotificationModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    llEmptyDetail.setVisibility(View.GONE);
                    cList = result.getList();
                    Collections.sort(cList, new Comparator<NotificationModel>() {
                        @Override
                        public int compare(NotificationModel item, NotificationModel item2) {
                            Date date = getDate(item.getTimestamp());
                            Date date2 = getDate(item2.getTimestamp());
                            return date2.compareTo(date);
                        }
                    });
                    adapter = new NotificationAdapter(getActivity(), cList, Notification.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    llEmptyDetail.setVisibility(View.VISIBLE);
//                    setErrorAdapter(lvMain, result.getOutputMsg());
                } else {
                    llEmptyDetail.setVisibility(View.VISIBLE);
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }


    }

    private Date getDate(long invoiceDate) {
        Date date = new Date();
        date.setTime(invoiceDate);
        return date;
    }

}
