package com.gennext.repairshop.user.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.ProfileActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.KeywordAdapter;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.ProfileModel;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.model.chat.MapModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.FieldValidation;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.ProgressButtonRounded;
import com.gennext.repairshop.util.RequestBuilder;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Arrays;

import static android.support.constraint.Constraints.TAG;

/**
 * Created by Admin on 7/4/2017.
 */

public class MyKeywords extends CompactFragment {


    ArrayList<String> keywordNames = new ArrayList<>(Arrays.asList("mobile accessories"));
    private ImageButton ibKeywordAdd;
    private EditText etKeyword;
    private KeywordAdapter mAdapter;
    private ProgressButtonRounded btnAction;
    private ApiRequest taskReq;
    private AlertDialog progressDialog;
    private String mPhotoStringLink = "";
    private ProfileModel profile;
    public boolean mItemAddedInList;
    private NestedScrollView nsvScroll;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    public static MyKeywords newInstance() {
        MyKeywords fragment = new MyKeywords();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_keywords, container, false);
        initToolBar(getActivity(), v, "My Wall");
        initUi(v);

        updateUi();
        return v;
    }


    private void updateUi() {

        mItemAddedInList = false;
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
                @Override
                public void onPreExecute() {
                    progressDialog = PopupProgress.newInstance(getContext()).show();
                }

                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    ProfileModel result = JsonParser.loadProfile(response);
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        updateProfile(result);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                    }
                }

                @Override
                public void onRetryClick() {
                    progressDialog.dismiss();
                    updateUi();
                }
            });
            String userId = AppUser.getUserId(getContext());
            taskReq.execute(ApiRequest.POST, AppSettings.GET_PROFILE, RequestBuilder.DefaultUser(userId));
    }

    private void updateProfile(ProfileModel result) {
        this.profile= result;


        if(!TextUtils.isEmpty(result.getKeywords())){
            if(result.getKeywords().contains(",")){
                keywordNames.clear();
                String[] key = result.getKeywords().split(",");
                for (String val : key){
                    keywordNames.add(val);
                }
                updateList();
            }else{
                keywordNames.clear();
                addKeywordInList(result.getKeywords());
            }
        }
    }

    private void initUi(View v) {
        nsvScroll = v.findViewById(R.id.nsv);
        ibKeywordAdd = (ImageButton) v.findViewById(R.id.ib_keyword_add);
        etKeyword = (EditText) v.findViewById(R.id.et_keyword_add);



        ibKeywordAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etKeyword.getText().toString().equals("")) {
                    mItemAddedInList = true;
                    addKeywordInList(etKeyword.getText().toString());
                    notifyDatasetChange();
                }
            }
        });

        btnAction = ProgressButtonRounded.newInstance(getContext(), v);

        btnAction.setText("Submit");
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeybord(getActivity());
                getFCMId();
            }
        });




//        // get the reference of RecyclerView
//        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
////        // set a StaggeredGridLayoutManager with 3 number of columns and horizontal orientation
////        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, LinearLayoutManager.HORIZONTAL);
//        // set a StaggeredGridLayoutManager with 3 number of columns and horizontal orientation
////        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager( 3, LinearLayoutManager.VERTICAL);
//
//        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
//        layoutManager.setFlexDirection(FlexDirection.ROW);
//        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
//        layoutManager.setAlignItems(AlignItems.STRETCH);
//        layoutManager.setFlexWrap(FlexWrap.WRAP);
//
//        recyclerView.setLayoutManager(layoutManager); // set LayoutManager to RecyclerView
//        //  call the constructor of CustomAdapter to send the reference and data to Adapter
//        mAdapter = new KeywordAdapter(getActivity(), keywordNames);
//        recyclerView.setAdapter(mAdapter); // set the Adapter to RecyclerView

        RecyclerView lvMain = (RecyclerView)v.findViewById(R.id.recyclerView);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new KeywordAdapter(getActivity(), keywordNames, MyKeywords.this);
        lvMain.setAdapter(mAdapter); // set the Adapter to RecyclerView
    }

    private void getFCMId() {
        executeTask();
    }

    private void executeTask() {

        if(profile==null){
            showToast(getContext(),"Something went wrong, Please try after some time.");
            return;
        }

        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                btnAction.startAnimation();
                etKeyword.requestFocus();
            }

            @Override
            public void onResponse(String response) {
                Model result = JsonParser.verifyOTP(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    mItemAddedInList = false;
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                }else{
                    taskReq.showErrorAlertBox(result.getOutput(),result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                btnAction.revertAnimation();
                executeTask();
            }
        });
        String userId = AppUser.getUserId(getContext());
        String name = profile.getFullName();
        String mobile = AppUser.getMobile(getActivity());
        String address= profile.getAddress();
        String pinCode= profile.getPinCode();
        String selectedLoc= profile.getLatLng();
        String keywords= getAllKeywords(keywordNames);
        String imageLink= profile.getImageUrl();
        String fcmId= profile.getGcmId();
        String macAddress= getMACAddress(getActivity());
        taskReq.execute(ApiRequest.POST, AppSettings.UPDATE_PROFILE, RequestBuilder.updateProfile(userId, name, mobile, address, pinCode, selectedLoc, keywords, imageLink, fcmId, macAddress));
    }

    private void animateButtonAndRevert() {
        Handler handler = new Handler();


        Runnable runnableRevert = new Runnable() {
            @Override
            public void run() {
                getFragmentManager().popBackStack();
            }
        };

        btnAction.revertSuccessAnimation();
        handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
    }

    private String getAllKeywords(ArrayList<String> keywordNames) {
        return TextUtils.join(",", keywordNames);
    }

    private void addKeywordInList(String keyword) {
        keywordNames.add(keyword);
        updateList();
    }

    private void updateList() {
//        mAdapter.notifyItemInserted(keywordNames.size()-1);
//        mAdapter.notifyItemRangeChanged(0, keywordNames.size());
        mAdapter.notifyDataSetChanged();
        etKeyword.setText("");
    }

    private void notifyDatasetChange() {
        mAdapter.notifyDataSetChanged();
        nsvScroll.post(new Runnable() {
            @Override
            public void run() {
                nsvScroll.scrollTo(0,nsvScroll.getBottom());
            }
        });
    }

}
