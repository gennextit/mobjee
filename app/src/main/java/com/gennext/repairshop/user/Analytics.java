package com.gennext.repairshop.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.util.AppAnimation;

/**
 * Created by Admin on 7/4/2017.
 */

public class Analytics extends CompactFragment implements View.OnClickListener{
    private ImageView ivProfile;
    private TextView tvProfileName;
    private ProgressBarAnim anim;
    private ProgressBar pBar1,pBar2,pBar3,pBar4;

    public static Fragment newInstance() {
        Analytics fragment=new Analytics();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_analytics,container,false);
        ((MainActivity)getActivity()).updateTitle("Analytics");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        LinearLayout llmenuOption4 = (LinearLayout) v.findViewById(R.id.ll_menu_option_4);
        LinearLayout llmenuOption3 = (LinearLayout) v.findViewById(R.id.ll_menu_option_3);
        LinearLayout llmenuOption2 = (LinearLayout) v.findViewById(R.id.ll_menu_option_2);
        LinearLayout llmenuOption1 = (LinearLayout) v.findViewById(R.id.ll_menu_option_1);
        pBar1 = (ProgressBar) v.findViewById(R.id.progressBar1);
        pBar2 = (ProgressBar) v.findViewById(R.id.progressBar2);
        pBar3 = (ProgressBar) v.findViewById(R.id.progressBar3);
        pBar4 = (ProgressBar) v.findViewById(R.id.progressBar4);

        llmenuOption4.setOnClickListener(this);
        llmenuOption3.setOnClickListener(this);
        llmenuOption2.setOnClickListener(this);
        llmenuOption1.setOnClickListener(this);
        llmenuOption1.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        AppAnimation.setProgressAnimate(pBar1,100,26);
        AppAnimation.setProgressAnimate(pBar2,100,66);
        AppAnimation.setProgressAnimate(pBar3,100,80);
        AppAnimation.setProgressAnimate(pBar4,100,20);

    }

    private void setProgressAnim(ProgressBar progressBar, int value) {
        progressBar.setMax(100 * 100);
        anim = new ProgressBarAnim(progressBar,value*100);
        anim.setDuration(1000);
        progressBar.startAnimation(anim);
    }

    public void updateUi() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_menu_option_1:
                addFragment(ClientDetails.newInstance(), "clientDetails");
                break;
            case R.id.ll_menu_option_2:
                break;
            case R.id.ll_menu_option_3:
                break;
            case R.id.ll_menu_option_4:
                break;
            case R.id.iv_profile:
                showToast(getContext(), "Update Later");
                break;
        }
    }

    public class ProgressBarAnim extends Animation {
        /* usage
        ProgressBarAnim anim = new ProgressBarAnim(progress, from, to);
        anim.setDuration(1000);
        progress.startAnimation(anim);
         */

        private ProgressBar progressBar;
        private float from;
        private float  to;

        public ProgressBarAnim(ProgressBar progressBar, int value) {
            this.progressBar = progressBar;
            this.from =(float)0;
            this.to =(float)value;
        }
        public ProgressBarAnim(ProgressBar progressBar, int from, int to) {
            super();
            this.progressBar = progressBar;
            this.from =(float)from;
            this.to =(float)to;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }

    }
}
