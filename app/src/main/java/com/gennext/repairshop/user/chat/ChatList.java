package com.gennext.repairshop.user.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gennext.repairshop.R;
import com.gennext.repairshop.UserChatActivity;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupDialog;
import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.FriendListAdapter;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.DBHelper;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Admin on 7/4/2017.
 */

public class ChatList extends CompactFragment {

    private ApiRequest taskReq;
    private RecyclerView lvMain;
    private ArrayList<ChatListModel> cList;
    private FriendListAdapter adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout llEmptyDetail;
    private DBHelper dbHelper;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            dbHelper.closeDB();
        }
    }

    public static ChatList newInstance() {
        ChatList fragment = new ChatList();
//        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_chat_list, container, false);
        dbHelper = new DBHelper(getContext());
        initUi(v);
        executeTask();
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateUi();
            }
        });

        llEmptyDetail = includeEmptyListView(v, "No new friends!", getString(R.string.chat_detail_message), R.drawable.ic_nav_chat);

    }

    private void hideProgressBar() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void refreshList() {
        updateUi();
    }

    public void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        updateUi();
    }


    private void updateUi() {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {

            }

            @Override
            public void onResponse(String response) {
                hideProgressBar();
                ChatListModel result = JsonParser.loadFriendList(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    ChatListModel dbList = dbHelper.getChatList(AppUser.getUserId(getContext()));
                    if (dbList.getOutput().equals(Const.SUCCESS)) {
                        ChatListModel finalRes = JsonParser.mergeChatList(result.getList(), dbList.getList());
                        cList = finalRes.getList();
                        Collections.sort(cList, new Comparator<ChatListModel>() {
                            @Override
                            public int compare(ChatListModel item, ChatListModel item2) {
                                Integer date = item.getCounter();
                                Integer date2 = item2.getCounter();
                                return date2.compareTo(date);
                            }
                        });
                        adapter = new FriendListAdapter(getActivity(), cList, ChatList.this);
                        lvMain.setAdapter(adapter);
                    } else {
                        cList = result.getList();
                        adapter = new FriendListAdapter(getActivity(), cList, ChatList.this);
                        lvMain.setAdapter(adapter);
                    }
                    hideEmptyListView(llEmptyDetail);

                } else if (result.getOutput().equals(Const.FAILURE)) {
//                    setErrorAdapter(lvMain, result.getOutputMsg());
                    showEmptyListView(llEmptyDetail);
                } else {
                    showEmptyListView(llEmptyDetail);
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                hideProgressBar();
                updateUi();
            }
        });
        String userId = AppUser.getUserId(getContext());
        taskReq.execute(ApiRequest.POST, AppSettings.GET_FRIEND_LIST, RequestBuilder.senderId(userId));

    }

    public void onItemSelect(ChatListModel item, int position) {
        dbHelper.deleteChatCounter(AppUser.getUserId(getContext()), item.getReceiverId());
        item.setCounter(0);
        cList.set(position, item);
        adapter.notifyDataSetChanged();
        Intent intent = new Intent(getActivity(), UserChatActivity.class);
        intent.putExtra("user", item);
        startActivity(intent);
    }

    public void onSearchTextChanged(CharSequence charSequence) {
        if (adapter != null) {
            adapter.getFilter().filter(charSequence.toString());
        }
    }

    public void onLongItemClicked(final ChatListModel item, final int itemPositionInList) {
        PopupDialog.newInstance("Alert", "Are you sure to delete " + item.getFullName() + " from your friend list?", new PopupDialog.DialogListener() {
            @Override
            public void onOkClick(DialogFragment dialog) {
                deleteFriend(item, itemPositionInList);
            }

            @Override
            public void onCancelClick(DialogFragment dialog) {

            }
        }).show(getFragmentManager(),"popupDialog");
    }

    private void deleteFriend(final ChatListModel item, final int position) {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {

            }

            @Override
            public void onResponse(String response) {
                hideProgressBar();
                Model result = JsonParser.manageDefault(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    removeItemFromList(item,position);
                    if (cList.size() == 0) {
                        showEmptyListView(llEmptyDetail);
                    }else{
                        hideEmptyListView(llEmptyDetail);
                    }
                } else if (result.getOutput().equals(Const.FAILURE)) {
//                    setErrorAdapter(lvMain, result.getOutputMsg());
                    showEmptyListView(llEmptyDetail);
                } else {
                    showEmptyListView(llEmptyDetail);
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                hideProgressBar();
                updateUi();
            }
        });
        String userId = AppUser.getUserId(getContext());
        taskReq.execute(ApiRequest.POST, AppSettings.DELETE_USER, RequestBuilder.deleteFriend(userId, item.getReceiverId()));
    }

    private void removeItemFromList(ChatListModel item, int position) {
        dbHelper.deleteChatCounter(AppUser.getUserId(getContext()), item.getReceiverId());
        cList.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, cList.size());
    }


}

