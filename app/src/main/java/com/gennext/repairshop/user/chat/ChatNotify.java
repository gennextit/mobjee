package com.gennext.repairshop.user.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.fcm.MyFirebaseMessagingService;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.JsonCreator;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.L;
import com.gennext.repairshop.util.RequestBuilder;

/**
 * Created by Admin on 7/4/2017.
 */

public class ChatNotify extends CompactFragment {

    private ApiRequest taskReq;
    private LinearLayout llMain;
    private TextView tvDetail;
    private String keyword,message;
    private ChatListModel userDetail;
    private String visibleText;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
        MyFirebaseMessagingService.chattingUserId = "";
    }

    public static ChatNotify newInstance(String visibleText, ChatListModel userDetail) {
        ChatNotify fragment=new ChatNotify();
        fragment.visibleText=visibleText;
        fragment.userDetail=userDetail;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_policy_details,container,false);
        MyFirebaseMessagingService.chattingUserId = userDetail.getReceiverId();
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        llMain = (LinearLayout) v.findViewById(R.id.ll_chat_detail);
        tvDetail = (TextView) v.findViewById(R.id.tv_chat_detail);

        if(!TextUtils.isEmpty(visibleText)){
            tvDetail.setText(visibleText);
            llMain.setVisibility(View.VISIBLE);
        }else{
            llMain.setVisibility(View.GONE);
        }
        keyword = Const.REPLIED_CHAT;
    }


    public void sendNotification(String msg) {
        this.message = msg;
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public void onResponse(String response) {
                Model result = JsonParser.defaultParser(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    L.m(result.getOutputMsg());
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    L.m(result.getOutputMsg());
                } else {
                    L.m(result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                sendNotification(message);
            }
        });
        String senderId = AppUser.getUserId(getContext());
//        String receiverIds = JsonCreator.generateReceiverId(userDetail);
        String receiverId = userDetail.getReceiverId();
        String gcmId = userDetail.getGcmId();
        taskReq.execute(ApiRequest.POST, AppSettings.SEND_CHAT, RequestBuilder.sendChatNotification(senderId, keyword, message, receiverId,gcmId));

    }


}
