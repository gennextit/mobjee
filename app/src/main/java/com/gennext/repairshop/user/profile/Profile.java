package com.gennext.repairshop.user.profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.ProfileActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.KeywordAdapter;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.ProfileModel;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.model.chat.MapModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.FieldValidation;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.ProgressButtonRounded;
import com.gennext.repairshop.util.RequestBuilder;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Arrays;

import static android.support.constraint.Constraints.TAG;

/**
 * Created by Admin on 7/4/2017.
 */

public class Profile extends CompactFragment {


    private MapModel location;
    private TextView tvLocation;
    ArrayList<String> keywordNames = new ArrayList<>(Arrays.asList("Mobile accessories"));
    private EditText etProfileName;
    private EditText etProfileAddress;
    private EditText etProfilePin;
    private ProgressButtonRounded btnAction;
    private ApiRequest taskReq;
    private AlertDialog progressDialog;
    private ImageView ivProfile;
    private String mPhotoStringLink = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    public static Profile newInstance() {
        Profile fragment = new Profile();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_profile, container, false);
        initToolBar(getActivity(), v, "Profile");
        initUi(v);

        updateUi();
        return v;
    }

    private void updateUi() {
        String image = AppUser.getImage(getContext());
        if(!TextUtils.isEmpty(image)) {
            updateImagePath(image);
        }
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
                @Override
                public void onPreExecute() {
                    progressDialog = PopupProgress.newInstance(getContext()).show();
                }

                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    ProfileModel result = JsonParser.loadProfile(response);
                    if (result.getOutput().equals(Const.SUCCESS)) {
                        updateProfile(result);
                    } else if (result.getOutput().equals(Const.FAILURE)) {
                        PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                                .show(getFragmentManager(), "popupAlert");
                    } else {
                        taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                    }
                }

                @Override
                public void onRetryClick() {
                    progressDialog.dismiss();
                    updateUi();
                }
            });
            String userId = AppUser.getUserId(getContext());
            taskReq.execute(ApiRequest.POST, AppSettings.GET_PROFILE, RequestBuilder.DefaultUser(userId));
    }

    private void updateProfile(ProfileModel result) {
        etProfileName.setText(result.getFullName());
        etProfileAddress.setText(result.getAddress());
        etProfilePin.setText(result.getPinCode());
        if(!TextUtils.isEmpty(result.getLatLng()) && result.getLatLng().contains(",")) {
            String[] latLng = result.getLatLng().split(",");
            location=new MapModel(latLng[0],latLng[1]);
        }
        String image = result.getImageUrl();
        if(!TextUtils.isEmpty(image)) {
            mPhotoStringLink = image;
            updateImagePath(image);
        }

        if(!TextUtils.isEmpty(result.getKeywords())){
            if(result.getKeywords().contains(",")){
                keywordNames.clear();
                String[] key = result.getKeywords().split(",");
                for (String val : key){
                    keywordNames.add(val);
                }
            }
        }
    }

    private void initUi(View v) {
        LinearLayout llPickupLoc = (LinearLayout) v.findViewById(R.id.ll_profile_location);
        ivProfile = (ImageView) v.findViewById(R.id.ivUserChat);
        tvLocation = (TextView) v.findViewById(R.id.tv_profile_location);
        etProfileName = (EditText) v.findViewById(R.id.et_profile_name);
        etProfileAddress = (EditText) v.findViewById(R.id.et_profile_address);
        etProfilePin = (EditText) v.findViewById(R.id.et_profile_pin);

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity)getActivity()).OptionImageAlert(getActivity());
            }
        });
        llPickupLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity) getActivity()).openLocationPicker();
            }
        });


        btnAction = ProgressButtonRounded.newInstance(getContext(), v);

        btnAction.setText("Submit");
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPhotoStringLink == null) {
                    showToast(getContext(), "Please select profile image");
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etProfileName)) {
                    return;
                } else if (!FieldValidation.isEmpty(getContext(), etProfilePin)) {
                    return;
                } else if (location == null) {
                    showToast(getContext(), "Please select location");
                    return;
                }
                hideKeybord(getActivity());
                getFCMId();
            }
        });
    }

    private void getFCMId() {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        executeTask(token);
                        // Log and toast
                        Log.d(TAG, token);
//                        Toast.makeText(getActivity(), token, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void executeTask(String token) {
        final String fcmId = token;

        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                btnAction.startAnimation();
            }

            @Override
            public void onResponse(String response) {
                Model result = JsonParser.verifyOTP(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    AppUser.setName(getContext(),etProfileName.getText().toString());
                    AppUser.setLatitude(getContext(),location.getLatitude());
                    AppUser.setLongitude(getContext(),location.getLongitude());
                    AppUser.setImage(getContext(),mPhotoStringLink);
                    animateButtonAndRevert();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    btnAction.revertAnimation();
                    PopupAlert.newInstance("Alert", result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                }else{
                    taskReq.showErrorAlertBox(result.getOutput(),result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                btnAction.revertAnimation();
                executeTask(fcmId);
            }
        });
        String userId = AppUser.getUserId(getContext());
        String name = etProfileName.getText().toString();
        String mobile = AppUser.getMobile(getActivity());
        String address= etProfileAddress.getText().toString();
        String pinCode= etProfilePin.getText().toString();
        String selectedLoc= location.getLatitude()+","+location.getLongitude();
        String keywords= getAllKeywords(keywordNames);
        String imageLink= mPhotoStringLink;
        String macAddress= getMACAddress(getActivity());
        taskReq.execute(ApiRequest.POST, AppSettings.UPDATE_PROFILE, RequestBuilder.updateProfile(userId, name, mobile, address, pinCode, selectedLoc, keywords, imageLink, fcmId, macAddress));
    }

    private void animateButtonAndRevert() {
        Handler handler = new Handler();


        Runnable runnableRevert = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        };

        btnAction.revertSuccessAnimation();
        handler.postDelayed(runnableRevert, Const.BUTTON_PROGRESS_TIME);
    }

    private String getAllKeywords(ArrayList<String> keywordNames) {
        String res = "";
        for (String value : keywordNames){
            res += value + ",";
        }
        return res.substring(0, res.length()-1);
    }


    public void setLocation(MapModel location) {
        this.location = location;
        tvLocation.setText(location.getAddress());
    }

    public void updateImagePath(String imageUrl) {
        this.mPhotoStringLink = imageUrl;
        Glide.with(getContext())
                .load(imageUrl).centerCrop()
                .transform(new CircleTransform(getContext()))
                .override(100,100).into(ivProfile);
    }
}
