package com.gennext.repairshop.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.ClientDetailAdapter;
import com.gennext.repairshop.model.PolicyAdapter;
import com.gennext.repairshop.model.ClientModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class ClientDetails extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<ClientModel> cList;
    private ClientDetailAdapter adapter;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }

    public static Fragment newInstance() {
        ClientDetails fragment = new ClientDetails();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_policy, container, false);
        initToolBar(getActivity(), v, "All Clients");
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });

        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN_COMPANY);
    }

    private void hideProgressBar() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void onItemSelect(ClientModel item) {
        addFragment(ClientProjectDetails.newInstance(item), "clientProjectDetails");
    }

    private class AssignTask extends AsyncTask<String, Void, ClientModel> {
        private Context context;
        HashMap<String, String> data;

        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ClientModel doInBackground(String... urls) {
            String response = null;
            String userId = AppUser.getUserId(context);
//            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(userId, ""));
            return getSampleList(getActivity());
        }


        @Override
        protected void onPostExecute(ClientModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    cList = result.getList();
                    adapter = new ClientDetailAdapter(getActivity(), cList, ClientDetails.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }


    }


    // Const params are must be same
    private ClientModel getSampleList(Context context) {
        ClientModel model = new ClientModel();
        model.setOutput(Const.SUCCESS);
        model.setList(new ArrayList<ClientModel>());
        model.getList().add(new ClientModel("Mr Arpit Dwivadi", "Rs.600000", "Approved", 60, getProjectSampleList4()));
        model.getList().add(new ClientModel("Mr Sunny Jaiswal", "Rs.750000", "Completed", 100, getProjectSampleList2()));
        model.getList().add(new ClientModel("Mr Rohit", "Rs.1500000", "Pending", 45, getProjectSampleList3()));
        model.getList().add(new ClientModel("Mr Abhijit Rao", "Rs.500000", "Cancelled", 20, getProjectSampleList1()));
        return model;
    }

    private ArrayList<ClientModel> getProjectSampleList4() {
        ArrayList<ClientModel> list = new ArrayList<>();
        list.add(new ClientModel("Construction in Delhi", "Rs.450000", "Approved", 71));
        list.add(new ClientModel("Gurugram Realstate project", "Rs.255000", "Pending", 45));
        list.add(new ClientModel("Audit in Noida Client", "Rs.200000", "Approved", 56));
        return list;
    }

    private ArrayList<ClientModel> getProjectSampleList1() {
        ArrayList<ClientModel> list = new ArrayList<>();
        list.add(new ClientModel("Project 1", "Rs.30000", "Cancelled", 20));
        list.add(new ClientModel("Project 2", "Rs.350000", "Cancelled", 15));
        list.add(new ClientModel("Project 3", "Rs.120000", "Cancelled", 24));
        return list;
    }

    private ArrayList<ClientModel> getProjectSampleList2() {
        ArrayList<ClientModel> list = new ArrayList<>();
        list.add(new ClientModel("Project 1", "Rs.450000", "Completed", 100));
        list.add(new ClientModel("Project 2", "Rs.255000", "Completed", 100));
        list.add(new ClientModel("Project 3", "Rs.350000", "Completed", 100));
        return list;
    }

    private ArrayList<ClientModel> getProjectSampleList3() {
        ArrayList<ClientModel> list = new ArrayList<>();
        list.add(new ClientModel("Project 1", "Rs.240000", "Pending", 35));
        list.add(new ClientModel("Project 2", "Rs.950000", "Approved", 56));
        list.add(new ClientModel("Project 3", "Rs.310000", "Pending", 45));
        return list;
    }



}
