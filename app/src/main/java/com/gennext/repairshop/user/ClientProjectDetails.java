package com.gennext.repairshop.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.model.ClientDetailProjectAdapter;
import com.gennext.repairshop.model.ClientModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class ClientProjectDetails extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<ClientModel> cList;
    private ClientDetailProjectAdapter adapter;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ClientModel mClientDetail;
    private TextView tvAmount;

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
    }
    public static Fragment newInstance(ClientModel mClientDetail) {
        ClientProjectDetails fragment=new ClientProjectDetails();
        AppAnimation.setFadeAnimation(fragment);
        fragment.mClientDetail=mClientDetail;
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_client_project_details,container,false);
        initToolBarForTheme(getActivity(),v,mClientDetail.getName());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        tvAmount = (TextView)v.findViewById(R.id.tv_clnt_amount);
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                executeTask();
                hideProgressBar();
            }
        });

        tvAmount.setText(mClientDetail.getAmount());

        cList = mClientDetail.getList();
        adapter = new ClientDetailProjectAdapter(getActivity(), cList, ClientProjectDetails.this);
        lvMain.setAdapter(adapter);
//        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.LOGIN_COMPANY);
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void onItemSelect(ClientModel item) {
        addFragment(ClientProjectUpdate.newInstance(item),"clientProjectUpdate");
    }

    private class AssignTask extends AsyncTask<String, Void, ClientModel> {
        private Context context;
        HashMap<String, String> data;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ClientModel doInBackground(String... urls) {
            String response = null;
            String userId= AppUser.getUserId(context);
//            response = ApiCall.POST(urls[0], RequestBuilder.LoginBody(userId, ""));
            return getSampleList(getActivity());
        }


        @Override
        protected void onPostExecute(ClientModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    cList = result.getList();
                    adapter = new ClientDetailProjectAdapter(getActivity(), cList, ClientProjectDetails.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }
        }


    }


    // Const params are must be same
    private ClientModel getSampleList(Context context) {
        ClientModel model = new ClientModel();
        model.setOutput(Const.SUCCESS);
        model.setList(new ArrayList<ClientModel>());
        model.getList().add(new ClientModel("Construction in Delhi", "Rs.500000", "Cancelled", 20));
        model.getList().add(new ClientModel("Gurugram Realstate project", "Rs.1600000", "Completed", 100));
        model.getList().add(new ClientModel("Audit in Noida Client", "Rs.1500000", "Pending", 45));
        return model;
    }


}
