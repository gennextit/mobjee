package com.gennext.repairshop.user.notification;

import android.Manifest;
import android.animation.Animator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gennext.repairshop.FullScreenImageActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.UserChatActivity;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupDialog;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.model.chat.CircleTransform;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.JsonCreator;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.RequestBuilder;
import com.gennext.repairshop.util.Sharewere;
import com.gennext.repairshop.util.Util;
import com.gennext.repairshop.util.WidgetAnimation;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * Created by Abhijit on 14-Oct-16.
 */

public class NotificationDetail extends CompactFragment implements View.OnClickListener {
    private TextView tvName, tvKeyword, tvAddress, tvMessage;
    private FragmentManager manager;
    private RelativeLayout RLView;
    private ImageView icActionCall;
    private ImageView icActionDirection, icActionChat;
    private ImageView ivImage;
    private NotificationModel item;
    private ApiRequest taskReq;
    private AlertDialog pDialog;
    private String mSenderId, mReceiverId, mFirebaseId;
    private Button btnReply;
    private ImageView ivMap;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    public static Fragment newinstance(NotificationModel item) {
        NotificationDetail fragment = new NotificationDetail();
        AppAnimation.setFadeAnimation(fragment);
        fragment.item = item;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_notification_detail, container, false);
        manager = getFragmentManager();
        InitUI(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        tvName.setText(item.getSenderName());
        if (!TextUtils.isEmpty(item.getSenderAddress())) {
            tvAddress.setText(item.getSenderAddress());
        } else {
            tvAddress.setVisibility(View.GONE);
        }
//        if(item.getKeyword().equalsIgnoreCase(Const.REPLIED_CHAT)){
//            tvKeyword.setText("Keyword : " + "Chat Reply");
//        }else {
//            tvKeyword.setText("Keyword : " + item.getKeyword());
//        }

        if (!TextUtils.isEmpty(item.getMessage())) {
            tvMessage.setText(item.getMessage());
        } else {
            tvMessage.setVisibility(View.GONE);
        }

//        if(!TextUtils.isEmpty(item.getSenderLocation()) && item.getSenderLocation().contains(",")){
//            String[] location = item.getSenderLocation().split("\\,");
//            Glide.with(getContext()).load(Util.local(location[0].trim(),location[1].trim()))
//                    .centerCrop()
//                    .into(ivMap);
//        }


        Glide.with(getContext())
                .load(item.getSenderImage())
                .centerCrop()
                .transform(new CircleTransform(getContext()))
                .override(100, 100)
                .placeholder(R.drawable.ic_profile_dark)
                .error(R.drawable.ic_profile_dark)
                .into(ivImage);
    }

    private void InitUI(View v) {
        RLView = (RelativeLayout) v.findViewById(R.id.llappFeedback);
        ivMap = (ImageView) v.findViewById(R.id.iv_popup);
        ivImage = (ImageView) v.findViewById(R.id.iv_popup_image);
        LinearLayout llClose = (LinearLayout) v.findViewById(R.id.ll_whitespace);

        tvName = (TextView) v.findViewById(R.id.tv_popup_title);
        tvAddress = (TextView) v.findViewById(R.id.tv_popup_detail);
        tvKeyword = (TextView) v.findViewById(R.id.tv_popup_keyword);
        tvMessage = (TextView) v.findViewById(R.id.tv_popup_message);
        icActionCall = (ImageView) v.findViewById(R.id.iv_action_call);
        icActionDirection = (ImageView) v.findViewById(R.id.iv_action_direction);
        icActionChat = (ImageView) v.findViewById(R.id.iv_action_chat);
        btnReply = (Button) v.findViewById(R.id.btn_reply);

        if(AppUser.getUserId(getContext()).equalsIgnoreCase(item.getSenderId())){
            icActionChat.setVisibility(View.GONE);
        }else{
            icActionChat.setVisibility(View.VISIBLE);
//            btnReply.setVisibility(View.VISIBLE);
        }

        icActionCall.setOnClickListener(this);
        icActionDirection.setOnClickListener(this);
        icActionChat.setOnClickListener(this);
        btnReply.setOnClickListener(this);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeybord(getActivity());
                manager.popBackStack();
            }
        });

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FullScreenImageActivity.class);
                intent.putExtra("nameUser", item.getSenderName());
                intent.putExtra("urlPhotoUser", "");
                intent.putExtra("urlPhotoClick", item.getSenderImage());
                startActivity(intent);
            }
        });

    }

    @Override
    public void onClick(View view) {
        setAnimation(view);
    }

    private void setAnimation(final View view) {
        WidgetAnimation.zoomInOutForRectButton(view, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                switch (view.getId()) {
                    case R.id.iv_action_call:
                        optionCall(item.getSenderMobile());
                        break;
                    case R.id.iv_action_direction:
                        String[] location = item.getSenderLocation().split("\\,");
                        Sharewere.openExternalNavagation(getActivity(), location[0], location[1], item.getSenderAddress());
                        break;
                    case R.id.iv_action_chat:
                        connectRequest();
                        break;
                    case R.id.btn_reply:
                        connectRequest();
                        break;

                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    private void connectRequest() {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                pDialog = PopupProgress.newInstance(getContext()).show();
            }

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Model result = JsonParser.defaultParser(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    mFirebaseId = result.getOutputMsg();
                    Intent intent = new Intent(getActivity(), UserChatActivity.class);
                    intent.putExtra("message", tvMessage.getText().toString());
                    intent.putExtra("user", getChatModel());
                    startActivity(intent);
                    getFragmentManager().popBackStack();
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                pDialog.dismiss();
                connectRequest();
            }
        });
        mSenderId = AppUser.getUserId(getContext());
        mReceiverId = item.getSenderId();
        mFirebaseId = mSenderId + mReceiverId;
        taskReq.execute(ApiRequest.POST, AppSettings.CONNECT_USER, RequestBuilder.connectChat(mSenderId, mReceiverId, mFirebaseId));
    }

    private ChatListModel getChatModel() {
        String name = item.getSenderName();
        String image = item.getSenderImage();
        return new ChatListModel(mSenderId, mReceiverId, mFirebaseId, name, image, "NA", "NA", "NA", item.getGcmId(),0,"1");
    }


    public void optionCall(final String number) {
        new TedPermission(getActivity())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        Sharewere.actionCall(getActivity(), number);
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        Toast.makeText(getActivity(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }


                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }

}