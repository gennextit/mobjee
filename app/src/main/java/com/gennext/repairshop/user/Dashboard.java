package com.gennext.repairshop.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.repairshop.ClientChatActivity;
import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.UserChatActivity;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.util.AppAnimation;
import com.gennext.repairshop.util.L;

/**
 * Created by Admin on 7/4/2017.
 */

public class Dashboard extends CompactFragment{
    private ImageView ivProfile;
    private TextView tvProfileName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        L.m("onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        L.m("onDetach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        L.m("onCreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        L.m("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        L.m("onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        L.m("onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        L.m("onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        L.m("onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        L.m("onDestroyView");
    }


    public static Dashboard newInstance() {
        Dashboard fragment=new Dashboard();
//        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_dashboard,container,false);
        L.m("onCreateView");
        initUi(v);
        return v;
    }


    private void initUi(View v) {

    }

    public void updateUi() {

    }

}
