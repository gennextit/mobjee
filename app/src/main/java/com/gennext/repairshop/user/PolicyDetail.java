package com.gennext.repairshop.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.util.AppAnimation;

/**
 * Created by Admin on 7/4/2017.
 */

public class PolicyDetail extends CompactFragment {

    public static Fragment newInstance() {
        PolicyDetail fragment=new PolicyDetail();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_policy_details,container,false);
        initUi(v);
        return v;
    }

    private void initUi(View v) {

    }


}
