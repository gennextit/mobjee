package com.gennext.repairshop.user;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gennext.repairshop.R;
import com.gennext.repairshop.global.CompactFragment;
import com.gennext.repairshop.global.PopupAlert;
import com.gennext.repairshop.global.PopupDialog;
import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.MyNeedAdapter;
import com.gennext.repairshop.model.NeedAdapter;
import com.gennext.repairshop.model.NeedModel;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.notification.NotificationDetail;
import com.gennext.repairshop.util.ApiCall;
import com.gennext.repairshop.util.ApiCallError;
import com.gennext.repairshop.util.ApiRequest;
import com.gennext.repairshop.util.AppSettings;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.DBHelper;
import com.gennext.repairshop.util.JsonParser;
import com.gennext.repairshop.util.RequestBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Admin on 7/4/2017.
 */

public class MyNeed extends CompactFragment {
    private RecyclerView lvMain;
    private ArrayList<NeedModel> cList;
    private MyNeedAdapter adapter;
    private AssignTask assignTask;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ApiRequest taskReq;
    private AlertDialog pDialog;
    private LinearLayout llEmptyList;
    private DBHelper dbHelper;


    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
        if (assignTask != null) {
            assignTask.onAttach(context);
        }
        if (taskReq != null) {
            taskReq.onAttach(context);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (assignTask != null) {
            assignTask.onDetach();
        }
        if (taskReq != null) {
            taskReq.onDetach();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dbHelper!=null){
            dbHelper.closeDB();
        }
    }

    public static MyNeed newInstance() {
        MyNeed fragment=new MyNeed();
//        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.user_my_need,container,false);
        initToolBar(getActivity(), v, "My Need");
        dbHelper = new DBHelper(getContext());
        initUi(v);
        return v;
    }

    private void initUi(View v) {
        lvMain = (RecyclerView)v.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeTask();
            }
        });
        llEmptyList = includeEmptyListView(v,"No new needs!",getString(R.string.need_detail_message),R.drawable.ic_nav_need);
        executeTask();
    }

    private void executeTask() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_ALLNEEDS);
    }

    public void refreshList() {
        assignTask = new AssignTask(getActivity());
        assignTask.execute(AppSettings.GET_ALLNEEDS);
    }

    private void hideProgressBar() {
        if(mSwipeRefreshLayout!=null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void revoveMyNeed(final NeedModel item, final int position) {
        PopupDialog.newInstance("Alert", "Are you sure to delete this need?", new PopupDialog.DialogListener() {
            @Override
            public void onOkClick(DialogFragment dialog) {
                removeNeed(item,position);
            }

            @Override
            public void onCancelClick(DialogFragment dialog) {

            }
        }).show(getFragmentManager(),"popupDialog");
    }


    private void removeNeed(final NeedModel item, final int position) {
        taskReq = ApiRequest.newInstance(getActivity(), getFragmentManager(), new ApiRequest.Listener() {
            @Override
            public void onPreExecute() {
                pDialog = PopupProgress.newInstance(getContext()).show();
            }

            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                Model result = JsonParser.defaultParser(response);
                if (result.getOutput().equals(Const.SUCCESS)) {
                    removeItemFromList(item,position);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    PopupAlert.newInstance(getString(R.string.alert), result.getOutputMsg(), PopupAlert.POPUP_DIALOG)
                            .show(getFragmentManager(), "popupAlert");
                } else {
                    taskReq.showErrorAlertBox(result.getOutput(), result.getOutputMsg());
                }
            }

            @Override
            public void onRetryClick() {
                pDialog.dismiss();
                removeNeed(item,position);
            }
        });
        String receiverId = AppUser.getUserId(getContext());
        String needId = item.getNeedId();
        taskReq.execute(ApiRequest.POST, AppSettings.DELETE_NEED, RequestBuilder.deleteNeed(receiverId, needId));
    }

    private void removeItemFromList(NeedModel item, int position) {
        dbHelper.deleteNeedCounter(AppUser.getUserId(getContext()),item.getNeedId());
        cList.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, cList.size());
    }



    private class AssignTask extends AsyncTask<String, Void, NeedModel> {
        private Context context;
        HashMap<String, String> data;
        public void onAttach(Context context) {
            // TODO Auto-generated method stub
            this.context = context;
        }

        public void onDetach() {
            // TODO Auto-generated method stub
            this.context = null;
        }

        public AssignTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected NeedModel doInBackground(String... urls) {
            String response = null;
            String userId= AppUser.getUserId(context);
            response = ApiCall.POST(urls[0], RequestBuilder.senderId(userId));

            return JsonParser.parseNeedByUserId(response,userId);
//            NeedModel dbList = dbHelper.getNeedList(AppUser.getUserId(context));
//            if(result.getOutput().equals(Const.SUCCESS) && dbList.getOutput().equals(Const.SUCCESS)){
//                return JsonParser.mergeNeedList(result.getList(),dbList.getList());
//            }else {
//                return result;
//            }
        }


        @Override
        protected void onPostExecute(NeedModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (context != null) {
                hideProgressBar();
                if (result.getOutput().equals(Const.SUCCESS)) {
                    hideEmptyListView(llEmptyList);
                    cList = result.getList();
                    Collections.sort(cList, new Comparator<NeedModel>() {
                        @Override
                        public int compare(NeedModel item, NeedModel item2) {
                            Date date = getDate(item.getTimestamp());
                            Date date2 = getDate(item2.getTimestamp());
                            return date2.compareTo(date);
                        }
                    });
                    adapter = new MyNeedAdapter(getActivity(), cList, MyNeed.this);
                    lvMain.setAdapter(adapter);
                } else if (result.getOutput().equals(Const.FAILURE)) {
                    showEmptyListView(llEmptyList);
//                    setErrorAdapter(lvMain, result.getOutputMsg());
                } else {
                    showEmptyListView(llEmptyList);
                    ApiCallError.newInstance(result.getOutput(), result.getOutputMsg(), new ApiCallError.ErrorListener() {
                        @Override
                        public void onErrorRetryClick(DialogFragment dialog) {
                            executeTask();
                        }

                        @Override
                        public void onErrorCancelClick(DialogFragment dialog) {

                        }
                    }).show(getFragmentManager(), "apiCallError");
                }
            }


        }

        private Date getDate(long invoiceDate) {
            Date date = new Date();
            date.setTime(invoiceDate);
            return date;
        }

    }


}
