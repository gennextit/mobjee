package com.gennext.repairshop.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.setting.Const;

import java.util.ArrayList;

public class DBManager {
	// Database Tokens
	SQLiteDatabase db;
	Activity act;

	public static DBManager newIsntance(Activity activity) {
		return new DBManager(activity);
	}

	public DBManager(Activity activity) {
		this.act = activity;
		CreateDataBase();
	}

	public void CreateDataBase() {
		db = act.openOrCreateDatabase(AppUser.COMMON, Context.MODE_PRIVATE, null);
		db.execSQL(
				"CREATE TABLE IF NOT EXISTS notification(userId VARCHAR, notificationId VARCHAR,readStatus INT);"); // 0 = false and 1 = true
		db.execSQL(
				"CREATE TABLE IF NOT EXISTS chat(userId VARCHAR, chatId VARCHAR,counter INT);");
		
	}

	/***************** Coach Database ****************************/

	public void insertNotificationCounter(String userId, String notificationId, int readStatus) {
		// Inserting record
		// Searching product
		Cursor c = db.rawQuery("SELECT * FROM notification WHERE userId='" + userId + "' AND notificationId='" + notificationId + "'", null);
		if (!c.moveToFirst()) {
			//record not found
			db.execSQL(
					"INSERT INTO notification ('userId','notificationId','readStatus') VALUES(" +
							"'" + userId + "','" + notificationId  + "','" + readStatus + "');");
		} else {
			//Modifying record if found
			db.execSQL("UPDATE notification SET readStatus='" + readStatus +
					"'WHERE userId='" +userId + "' AND notificationId='" +notificationId + "'");
		}
	}

	public Boolean deleteNotificationCounter(String userId, String notificationId) {
		// Searching product
		Cursor c = db.rawQuery("SELECT * FROM notification WHERE userId='" + userId + "' AND notificationId='" + notificationId + "'", null);
		if (c.moveToFirst()) {
			// Deleting record if found
			db.execSQL("DELETE FROM notification WHERE userId='" + userId + "'AND notificationId='" + notificationId + "'");
			return true;
		} else {
			return false;
		}
	}

	public NotificationModel getNotificationList(String userId) {
		NotificationModel result = new NotificationModel();
		//Cursor c = db.rawQuery("SELECT * FROM product ORDER BY id DESC", null);
		Cursor c = db.rawQuery("SELECT * FROM notification WHERE userId='" + userId + "'", null);
		// Checking if no records found
		if (c.getCount() == 0) {
			result.setOutput(Const.FAILURE);
			result.setOutput("No record found");
			if(!c.isClosed()){
				c.close();
			}
			return result;
		}
		result.setOutput(Const.SUCCESS);
		result.setList(new ArrayList<NotificationModel>());
		while (c.moveToNext()) {
			NotificationModel model = new NotificationModel();
			model.setNotificationId(c.getString(1));
			model.setReadStatus(c.getInt(2));
			result.getList().add(model);
		}
		if(!c.isClosed()){
			c.close();
		}
		return result;
	}



	public void CloseDB() {
		db.close();
	}
}
