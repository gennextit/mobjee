package com.gennext.repairshop.util;

/**
 * Created by Abhijit on 13-Dec-16.
 */

/**
 * ERROR-101 (Unformetted Json Response)
 * ERROR-102 ()
 */
public class AppSettings {

    public static final Double TEST_VERSION_NAME = 3.8;
    //    public static final String SERVER = "http://techlyt.com";
    public static final String SERVER = "http://mobjee.com";
    public static final String COMMON = SERVER+ "/mobapp/";

    public static final String SEND_FEEDBACK  = COMMON + "sendFeedback "; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String TERMS_OF_USE  = COMMON + "mobjee_terms_and_conditions.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook
    public static final String PRIVACY_PACY_POLICY  = COMMON + "mobjee_privacy_policy.pdf"; //personName,bankId,branch,accountNumber,ifscCode,userId,checkbookPassbook


    public static final String REPORT_SERVER_ERROR = COMMON + "serverErrorReporting";
    public static String SHARE_URL="http://bdoindia.app.link/category?";//https://www.appsflyer.com
    public static String PLAYSTORE_URL="https://play.google.com/store/apps/details?id=";//https://www.appsflyer.com

    public static final String LOGIN = COMMON + "userRegistration/setuserRegistrationByMobile";//parameters: mobileNumber, macAddr
    public static final String VERIFY_OTP = COMMON + "userRegistration/verifyOtp";//parameters: mobileNumber, otp
    public static final String UPDATE_PROFILE = COMMON + "userRegistration/updateuserProfileByM";//parameters: entry, candidateId, shopName, pinCode, address, city, location, latitudeLongitude, mobileNumber, about, keywords, image, macAddr, gcmId
    public static final String GET_PROFILE = COMMON + "userRegistration/getCandidateDetails";//parameter: candidateId
    public static final String CONNECT_USER = COMMON + "Messaging/sendU2UMessage";//parametrs: senderId, receiverId, firebaseId
    public static final String DELETE_USER = COMMON + "Messaging/deleteFriend";//parametrs: senderId, receiverId
    public static final String GET_FRIEND_LIST = COMMON + "Messaging/getFriendList";//parameters: senderId
    public static final String GET_NOTIFICATION = COMMON + "Notification/receiveU2UNotification";//parameters: receiverId
    public static final String SEND_NOTIFICATION = COMMON + "Notification/sendU2UNotification";//parameters: senderId, receiverIds, keyword, message,
    public static final String SEND_CHAT = COMMON + "Notification/sendU2UChat";//parameters: senderId, receiverId, gcmId, keyword, message,
    public static final String DELETE_NOTIFICATION = COMMON + "Notification/deleteU2UNotification";//parameters: receiverId, notificationId
    public static final String FIND_ACC_OWNER = COMMON + "Search/findAccOwner";//parameters: senderId

    public static final String GET_ALLNEEDS = COMMON + "Needs/getAllNeeds";//parameters: no params
    public static final String ADD_NEED = COMMON + "Needs/addNeed";//parameters: senderId, need
    public static final String DELETE_NEED = COMMON + "Needs/deleteNeed";//parameters: senderId, needId



    public static final String LOGIN_COMPANY = COMMON + "";
    public static final String LOGIN_EMPLOYEE = COMMON + "";
    public static final String GET_COMPANIES_LIST = COMMON + "";
    public static final String SIGNUP_COMPANY = COMMON + "";
}

/* New API's required for BizzzWizzz Consultant App


*/

/* New API's required for BizzzWizzz App

API 1:   http://bizzzwizzz.com/index.php/Rest/signup

Params: name,mobile,email,panNo,pass,profileType

Response: success/failure.(return success and basic info similar to login api)

*/

/*
    1 1xx Informational.
    2 2xx Success.
    3 3xx Redirection.
    4 4xx Client Error.
    5 5xx Server Error.
*/
