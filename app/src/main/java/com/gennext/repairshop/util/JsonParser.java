package com.gennext.repairshop.util;


import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.CompanyModel;
import com.gennext.repairshop.model.EmployeesModel;
import com.gennext.repairshop.model.Model;
import com.gennext.repairshop.model.NeedModel;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.model.ProfileModel;
import com.gennext.repairshop.setting.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";
    public static final String ERROR_CODE = "ERROR-101";
    public static final String ERROR_MSG = "ERROR-101 No msg available";
    public static final String SERVER = ApiCallError.SERVER_ERROR;
    public static final String INTERNET = ApiCallError.INTERNET_ERROR;
    public static final String IO_EXCEPTION = ApiCall.IO_EXCEPTION;
    public static final String JSON_ERROR = ApiCallError.JSON_ERROR;

    //default constant variable
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";


    public static Model defaultSuccessResponse(String response) {
        Model model = new Model();
        model.setOutput(Const.SUCCESS);
        model.setOutputMsg("Attendance Marked successful");
        return model;
    }

    public static Model defaultParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    private static String[] defaultParserModel(String response) {
        String output = "", outputMsg = "";
        if (response.contains(IO_EXCEPTION)) {
            output = INTERNET;
            outputMsg = response;
            return new String[]{output, outputMsg};
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        output = Const.SUCCESS;
                        outputMsg = mainObject.getString(MESSAGE);
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        output = Const.FAILURE;
                        outputMsg = mainObject.getString(MESSAGE);
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    output = SERVER;
                    outputMsg = e.toString() + "\n" + response;
                }
            } else {
                ERRORMESSAGE = response;
                output = SERVER;
                outputMsg = response;
            }
        }
        return new String[]{output, outputMsg};
    }

    public static Model manageDefault(String response) {
        Model model = new Model();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
//        model.setList(new ArrayList<Model>());
//        try {
//            JSONArray mainArray=new JSONArray(model.getOutputMsg());
//            for (int i=0;i<mainArray.length();i++){
//                JSONObject mainObj=mainArray.getJSONObject(i);
//                CompanyModel item = new CompanyModel();
//                item.setId(mainObj.optString("id"));
//                item.setName(mainObj.optString("companyName"));
//                item.setEmail(mainObj.optString("email"));
//                model.getList().add(item);
//            }
//
//        } catch (JSONException e) {
//            model.setOutput(JSON_ERROR);
//            model.setOutputMsg(e.toString() + "\n" + response);
//        }
        return model;
    }

    public static Model companyLogin(String response) {
        return defaultParser(response);
    }

    public static EmployeesModel employeeLogin(String response) {
        return null;
    }

    public static CompanyModel getCompanyList(String response) {
        return null;
    }

    public static Model verifyOTP(String response) {
        return defaultParser(response);
    }

    public static ProfileModel loadProfile(String response) {
        ProfileModel model = new ProfileModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            JSONObject mainObj = mainArray.getJSONObject(0);
            model.setCandidateId(mainObj.optString("candidateId"));
            model.setLatLng(mainObj.optString("locationLatitudeLongitude"));
            model.setFullName(mainObj.optString("fullName"));
            model.setAddress(mainObj.optString("address"));
            model.setPinCode(mainObj.optString("pinCode"));
            model.setMobileNumber(mainObj.optString("mobileNumber"));
            model.setImageUrl(mainObj.optString("imageUrl"));
            model.setKeywords(mainObj.optString("keywords"));
            model.setGcmId(mainObj.optString("gcmId"));

        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static ChatListModel loadFriendList(String response) {
        ChatListModel model = new ChatListModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<ChatListModel>());
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject mainObj = mainArray.getJSONObject(i);
                if(mainObj.optString("status").equals("1")) {
                    ChatListModel item = new ChatListModel();
                    item.setSenderId(mainObj.optString("senderId"));
                    item.setReceiverId(mainObj.optString("receiverId"));
                    item.setFirebaseId(mainObj.optString("firebaseId"));
                    item.setLatLng(mainObj.optString("locationLatitudeLongitude"));
                    item.setFullName(mainObj.optString("fullName").equals("") ? "Not Aval" : mainObj.optString("fullName"));
                    item.setMobileNumber(mainObj.optString("mobileNumber"));
                    item.setAddress(mainObj.optString("address"));
                    item.setImageUrl(mainObj.optString("imageUrl"));
                    item.setGcmId(mainObj.optString("gcmId"));
                    item.setStatus(mainObj.optString("status"));
                    item.setCounter(0);
                    model.getList().add(item);
                }
            }


        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static ProfileModel loadSearchResult(String response) {
        ProfileModel model = new ProfileModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<ProfileModel>());
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject mainObj = mainArray.getJSONObject(i);
                ProfileModel item = new ProfileModel();
                item.setCandidateId(mainObj.optString("candidateId"));
                item.setLatLng(mainObj.optString("locationLatitudeLongitude"));
                item.setFullName(mainObj.optString("fullName"));
                item.setAddress(mainObj.optString("address"));
                item.setPinCode(mainObj.optString("pinCode"));
                item.setMobileNumber(mainObj.optString("mobileNumber"));
                item.setImageUrl(mainObj.optString("imageUrl"));
                item.setKeywords(mainObj.optString("keywords"));
                item.setDistance(mainObj.optString("km"));
                item.setRadius(mainObj.optString("radius"));
                item.setGcmId(mainObj.optString("gcmId"));
                model.getList().add(item);
            }


        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static NotificationModel parseNotification(String response) {
        NotificationModel model = new NotificationModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<NotificationModel>());
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject mainObj = mainArray.getJSONObject(i);
                NotificationModel item = new NotificationModel();
                item.setNotificationId(mainObj.optString("notificationId"));
                item.setTimestamp(DateTimeUtility.convertTimeInMiliSec(mainObj.optString("timeStamp")));
                item.setSenderId(mainObj.optString("senderId"));
                item.setSenderName(mainObj.optString("fullName"));
                item.setSenderAddress(mainObj.optString("address"));
                item.setSenderMobile(mainObj.optString("mobileNumber"));
                item.setSenderImage(mainObj.optString("imageUrl"));
                item.setSenderLocation(mainObj.optString("locationLatitudeLongitude"));
                item.setKeyword(mainObj.optString("keyword"));
                item.setMessage(mainObj.optString("message"));
                item.setGcmId(mainObj.optString("gcmId"));
                item.setReadStatus(Const.NOT_READED);
                model.getList().add(item);
            }


        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }

    public static NeedModel parseNeed(String response) {
        NeedModel model = new NeedModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<NeedModel>());
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject mainObj = mainArray.getJSONObject(i);
                NeedModel item = new NeedModel();
                item.setNeedId(mainObj.optString("needId"));
                item.setTimestamp(DateTimeUtility.convertTimeInMiliSec(mainObj.optString("timeAdded")));
                item.setUserId(mainObj.optString("senderId"));
                item.setName(mainObj.optString("fullName"));
                item.setAddress(mainObj.optString("address"));
                item.setMobile(mainObj.optString("mobileNumber"));
                item.setImage(mainObj.optString("imageUrl"));
                item.setLocation(mainObj.optString("locationLatitudeLongitude"));
                item.setDetail(mainObj.optString("need"));
                item.setKeywords(mainObj.optString("keywords"));
                item.setGcmId(mainObj.optString("gcmId"));
                item.setReadStatus(0);
                model.getList().add(item);
            }


        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }


     public static NeedModel parseNeedByUserId(String response, String userId) {
        NeedModel model = new NeedModel();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if (!model.getOutput().equals(Const.SUCCESS)) {
            return model;
        }
        //write your logic here for success response
        model.setList(new ArrayList<NeedModel>());
        try {
            JSONArray mainArray = new JSONArray(model.getOutputMsg());
            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject mainObj = mainArray.getJSONObject(i);
                if(mainObj.optString("senderId").equalsIgnoreCase(userId)) {
                    NeedModel item = new NeedModel();
                    item.setNeedId(mainObj.optString("needId"));
                    item.setTimestamp(DateTimeUtility.convertTimeInMiliSec(mainObj.optString("timeAdded")));
                    item.setUserId(mainObj.optString("senderId"));
                    item.setName(mainObj.optString("fullName"));
                    item.setAddress(mainObj.optString("address"));
                    item.setMobile(mainObj.optString("mobileNumber"));
                    item.setImage(mainObj.optString("imageUrl"));
                    item.setLocation(mainObj.optString("locationLatitudeLongitude"));
                    item.setDetail(mainObj.optString("need"));
                    item.setKeywords(mainObj.optString("keywords"));
                    item.setGcmId(mainObj.optString("gcmId"));
                    item.setReadStatus(0);
                    model.getList().add(item);
                }
            }


        } catch (JSONException e) {
            model.setOutput(JSON_ERROR);
            model.setOutputMsg(e.toString() + "\n" + response);
        }
        return model;
    }


    public static NotificationModel mergeList(ArrayList<NotificationModel> serverList, ArrayList<NotificationModel> localList) {
        NotificationModel result = new NotificationModel();
        result.setOutput(Const.SUCCESS);
        for (NotificationModel model : localList) {
            for (NotificationModel item : serverList) {
                if (model.getNotificationId().equalsIgnoreCase(item.getNotificationId())) {
                    item.setReadStatus(model.getReadStatus());
                    break;
                }
            }
        }
        result.setList(serverList);
        return result;
    }


    public static NeedModel mergeNeedList(ArrayList<NeedModel> serverList, ArrayList<NeedModel> localList) {
        NeedModel result = new NeedModel();
        result.setOutput(Const.SUCCESS);
        for (NeedModel model : localList) {
            for (NeedModel item : serverList) {
                if (model.getNeedId().equalsIgnoreCase(item.getNeedId())) {
                    item.setReadStatus(model.getReadStatus());
                    break;
                }
            }
        }
        result.setList(serverList);
        return result;
    }


    public static ChatListModel mergeChatList(ArrayList<ChatListModel> serverList, ArrayList<ChatListModel> localList) {
        ChatListModel result = new ChatListModel();
        result.setOutput(Const.SUCCESS);
        for (ChatListModel model : localList) {
            for (ChatListModel item : serverList) {
                if (model.getReceiverId().equalsIgnoreCase(item.getReceiverId())) {
                    item.setCounter(model.getCounter());
                    break;
                }
            }
        }
        result.setList(serverList);
        return result;
    }


}