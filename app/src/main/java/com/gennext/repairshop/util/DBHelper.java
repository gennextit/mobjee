package com.gennext.repairshop.util;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.LoginModel;
import com.gennext.repairshop.model.NeedModel;
import com.gennext.repairshop.model.NotificationModel;
import com.gennext.repairshop.model.chat.ChatModel;
import com.gennext.repairshop.model.chat.UserModel;
import com.gennext.repairshop.setting.Const;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.gennext.repairshop.util.DBHelperConst.KEY_COUNTER;
import static com.gennext.repairshop.util.DBHelperConst.KEY_CREATED_AT;
import static com.gennext.repairshop.util.DBHelperConst.KEY_EMAIL;
import static com.gennext.repairshop.util.DBHelperConst.KEY_FRIEND_ID;
import static com.gennext.repairshop.util.DBHelperConst.KEY_ID;
import static com.gennext.repairshop.util.DBHelperConst.KEY_NEED_ID;
import static com.gennext.repairshop.util.DBHelperConst.KEY_NOTIFICATION_ID;
import static com.gennext.repairshop.util.DBHelperConst.KEY_PASSWORD;
import static com.gennext.repairshop.util.DBHelperConst.KEY_READ_STATUS;
import static com.gennext.repairshop.util.DBHelperConst.KEY_USER_ID;


public class DBHelper extends SQLiteOpenHelper {
    // Logcat tag
    private static final String LOG = "DBHelper";
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = AppUser.COMMON + "_DB";
    private final SQLiteDatabase db;
    // Table Names
    private static final String TABLE_NOTIFICATION = "notification";
    private static final String TABLE_CHAT = "chat";
    private static final String TABLE_NEED = "need";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.db = getWritableDatabase();
    }


    // Table Create Statements
    // Todo table create statement

    private static final String CREATE_NOTIFICATION = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NOTIFICATION + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_USER_ID + " TEXT," + KEY_NOTIFICATION_ID + " TEXT," + KEY_READ_STATUS + " INTEGER," + KEY_CREATED_AT
            + " DATETIME" + ")";

    private static final String CREATE_NEED = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NEED + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_USER_ID + " TEXT," + KEY_NEED_ID + " TEXT," + KEY_READ_STATUS + " INTEGER," + KEY_CREATED_AT
            + " DATETIME" + ")";

    private static final String CREATE_CHAT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_CHAT + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_USER_ID + " TEXT," + KEY_FRIEND_ID + " TEXT," + KEY_COUNTER + " INTEGER," + KEY_CREATED_AT
            + " DATETIME" + ")";

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_NOTIFICATION);
        db.execSQL(CREATE_CHAT);
        db.execSQL(CREATE_NEED);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEED);
        // create new tables
        onCreate(db);
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    // ------------------------ "todos" table methods ----------------//
    public void insertNotificationCounter(String userId, String notificationId, int readStatus) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_NOTIFICATION_ID + " = '" + notificationId + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userId);
        values.put(KEY_NOTIFICATION_ID, notificationId);
        values.put(KEY_READ_STATUS, readStatus);
        values.put(KEY_CREATED_AT, getDateTime());
        if (!c.moveToFirst()) {
            // insert row
            db.insert(TABLE_NOTIFICATION, null, values);
        } else {
            // updating row
            int res = db.update(TABLE_NOTIFICATION, values, KEY_USER_ID + " = ? AND " + KEY_NOTIFICATION_ID + " = ?",
                    new String[]{userId, notificationId});

        }
        closeDB();
    }

    public void insertNeedCounter(String userId, String needId, int readStatus) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NEED
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_NEED_ID + " = '" + needId + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userId);
        values.put(KEY_NEED_ID, needId);
        values.put(KEY_READ_STATUS, readStatus);
        values.put(KEY_CREATED_AT, getDateTime());
        if (!c.moveToFirst()) {
            // insert row
            db.insert(TABLE_NEED, null, values);
        } else {
            // updating row
            int res = db.update(TABLE_NEED, values, KEY_USER_ID + " = ? AND " + KEY_NEED_ID + " = ?",
                    new String[]{userId, needId});

        }
        closeDB();
    }

    public void insertChatCounter(String userId, String friendId, int counter) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_CHAT
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_FRIEND_ID + " = '" + friendId + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userId);
        values.put(KEY_FRIEND_ID, friendId);
        values.put(KEY_COUNTER, counter);
        values.put(KEY_CREATED_AT, getDateTime());
        if (!c.moveToFirst()) {
            // insert row
            db.insert(TABLE_CHAT, null, values);
        } else {
            // updating row
            db.execSQL("UPDATE chat SET counter = counter +1 WHERE userId='" +userId + "' AND friendId='" +friendId + "'");

//            int res = db.update(TABLE_CHAT, values, KEY_USER_ID + " = ? AND " + KEY_FRIEND_ID + " = ?",
//                    new String[]{userId, friendId});

        }
        closeDB();
    }

    public Boolean deleteNotificationCounter(String userId, String notificationId) {
        SQLiteDatabase db = this.getReadableDatabase();
        int temp = db.delete(TABLE_NOTIFICATION, KEY_USER_ID + " = ? AND " + KEY_NOTIFICATION_ID + " = ?",
                new String[]{userId, notificationId});
        closeDB();
        if (temp == 1) {
            return true;
        } else {
           return false;
        }
    }

    public Boolean deleteNeedCounter(String userId, String needId) {
        SQLiteDatabase db = this.getReadableDatabase();
        int temp = db.delete(TABLE_NEED, KEY_USER_ID + " = ? AND " + KEY_NEED_ID + " = ?",
                new String[]{userId, needId});
        closeDB();
        if (temp == 1) {
            return true;
        } else {
           return false;
        }
    }

    public Boolean deleteChatCounter(String userId, String friendId) {
        SQLiteDatabase db = this.getReadableDatabase();
        int temp = db.delete(TABLE_CHAT, KEY_USER_ID + " = ? AND " + KEY_FRIEND_ID + " = ?",
                new String[]{userId, friendId});
        closeDB();
        if (temp == 1) {
            return true;
        } else {
           return false;
        }
    }

    public NotificationModel getNotificationList(String userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
		SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        NotificationModel result = new NotificationModel();
        result.setList(new ArrayList<NotificationModel>());
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            do {
                NotificationModel model = new NotificationModel();
                model.setNotificationId(c.getString(c.getColumnIndex(KEY_NOTIFICATION_ID)));
                model.setReadStatus(c.getInt(c.getColumnIndex(KEY_READ_STATUS)));
                result.getList().add(model);
            } while (c.moveToNext());
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        closeDB();
        return result;
    }

     public NeedModel getNeedList(String userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NEED
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
		SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        NeedModel result = new NeedModel();
        result.setList(new ArrayList<NeedModel>());
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            do {
                NeedModel model = new NeedModel();
                model.setNeedId(c.getString(c.getColumnIndex(KEY_NEED_ID)));
                model.setReadStatus(c.getInt(c.getColumnIndex(KEY_READ_STATUS)));
                result.getList().add(model);
            } while (c.moveToNext());
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        closeDB();
        return result;
    }

     public ChatListModel getChatList(String userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_CHAT
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
		SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        ChatListModel result = new ChatListModel();
        result.setList(new ArrayList<ChatListModel>());
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            do {
                ChatListModel model = new ChatListModel();
                model.setReceiverId(c.getString(c.getColumnIndex(KEY_FRIEND_ID)));
                model.setCounter(c.getInt(c.getColumnIndex(KEY_COUNTER)));
                result.getList().add(model);
            } while (c.moveToNext());
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        closeDB();
        return result;
    }

    public int getChatCounter(Context context) {
        String userId = AppUser.getUserId(context);
        String selectQuery = "SELECT  * FROM " + TABLE_CHAT
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
//        if (c.moveToFirst()) {
//            do {
//                count = count+1;
//            } while (c.moveToNext());
//        }
        int count = c.getCount();
        closeDB();
        return count;
    }

    public LoginModel addNewMember(String userId, LoginModel result) {
        if (result == null) {
            result = new LoginModel();
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Member detail doesn't exist");
            return result;
        }
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_PASSWORD + " = '" + result.getPassword() + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (!c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_ID, userId);
            values.put(KEY_EMAIL, result.getEmail());
            values.put(KEY_PASSWORD, result.getPassword());
            values.put(KEY_CREATED_AT, getDateTime());

            // insert row
            long todo_id = db.insert(TABLE_NOTIFICATION, null, values);
            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_INSERTED);
            result.setId(String.valueOf(todo_id));
            closeDB();
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Member mobile already exist");
            closeDB();
            return result;
        }
    }

    public LoginModel updateNewMember(String userId, LoginModel result) {

        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_ID + " = '" + result.getId() + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_ID, userId);
            values.put(KEY_EMAIL, result.getEmail());
            values.put(KEY_PASSWORD, result.getPassword());
            values.put(KEY_CREATED_AT, getDateTime());
            // updating row
            int res = db.update(TABLE_NOTIFICATION, values, KEY_ID + " = ? AND " + KEY_USER_ID + " = ?",
                    new String[]{result.getId(), userId});

            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_UPDATED);
            closeDB();
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Employee does not exist");
            closeDB();
            return result;
        }
    }

    public LoginModel getAllNewMember(String userId) {
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_ID + " DESC";
//		SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        LoginModel result = new LoginModel();
        result.setList(new ArrayList<LoginModel>());
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            do {
                LoginModel model = new LoginModel();
                model.setId(c.getString(c.getColumnIndex(KEY_ID)));
                model.setUserId(c.getString(c.getColumnIndex(KEY_USER_ID)));
                model.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
                model.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
                model.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
                result.getList().add(model);
            } while (c.moveToNext());
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        closeDB();
        return result;
    }

    public LoginModel deleteNewMember(String userId, String id) {
        LoginModel result = new LoginModel();
        int temp = db.delete(TABLE_NOTIFICATION, KEY_ID + " = ? AND " + KEY_USER_ID + " = ?",
                new String[]{id, userId});
        if (temp == 1) {
            result.setOutput(Const.SUCCESS);
            result.setOutputMsg(Const.RECORD_DELETED_SUCCESSFUL);
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg(Const.NO_RECORD_FOUND);
        }
        return result;
    }

    public LoginModel memberLogin(String userId, String password) {
        LoginModel result = new LoginModel();

        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION
                + " WHERE " + KEY_USER_ID + " = '" + userId + "'"
                + " AND " + KEY_PASSWORD + " = '" + password + "'";
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            result.setOutput(Const.SUCCESS);
            result.setId(c.getString(c.getColumnIndex(KEY_ID)));
            result.setId(c.getString(c.getColumnIndex(KEY_ID)));
            result.setUserId(c.getString(c.getColumnIndex(KEY_USER_ID)));
            result.setEmail(c.getString(c.getColumnIndex(KEY_EMAIL)));
            result.setPassword(c.getString(c.getColumnIndex(KEY_PASSWORD)));
            result.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
            return result;
        } else {
            result.setOutput(Const.FAILURE);
            result.setOutputMsg("Login credentials doesn't match");
            return result;
        }
    }


}


