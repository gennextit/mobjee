package com.gennext.repairshop.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Admin on 4/16/2018.
 */

public class FileUtils {
    public static final String FOLDER_SHARE = "Share";

    public static Uri getImageUri(Context inContext, Bitmap inImage, String imageName) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, imageName, null);
        return Uri.parse(path);
    }

    public static Uri getUriToDrawable(Context context, int image) {
        Bitmap bm = BitmapFactory.decodeResource( context.getResources(), image);
        //String filename = mItem.getTitle() +".png";
        String filename = getTimeStamp() +".png";
        // step 2
        File filePath = new File(context.getExternalFilesDir(FOLDER_SHARE), filename);

        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(filePath);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri imguri= Uri.fromFile(filePath);
        return imguri;
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat("yyyy_MM_dd_HHmmss", Locale.getDefault()).format(new Date());
    }

}
