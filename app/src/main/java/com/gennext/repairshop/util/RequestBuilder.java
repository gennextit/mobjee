package com.gennext.repairshop.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */




public class RequestBuilder {
    public static RequestBody Default(String candidateId) {
        return new FormBody.Builder()
                .addEncoded("candidateId", candidateId)
                .build();
    }
    public static List<KeyValuePair> DefaultUser(String candidateId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("candidateId",candidateId));
        return params;
    }

    public static List<KeyValuePair> senderId(String senderId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        return params;
    }

     public static List<KeyValuePair> receiverId(String receiverId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("receiverId",receiverId));
        return params;
    }

    public static List<KeyValuePair> deleteNotification(String receiverId, String notificationId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("receiverId",receiverId));
        params.add(new KeyValuePair("notificationId",notificationId));
        return params;
    }

    public static List<KeyValuePair> deleteNeed(String senderId, String needId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        params.add(new KeyValuePair("needId",needId));
        return params;
    }

    public static RequestBody getRequestBody(List<KeyValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (KeyValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
//        RequestBody formBody = builder.build();
        return builder.build();
    }

    public static RequestBody uploadMultipleImages(String userId, List<File> file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            for (File model : file) {
                builder.addFormDataPart("image[]", model.getName(),
                        RequestBody.create(MediaType.parse("text/plain"), model));
            }
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody uploadSingleImages(String userId, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            builder.addFormDataPart("image", file.getName(),
                    RequestBody.create(MediaType.parse("text/plain"), file));
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody FeedbackDetail(String consultantId, String feedback) {
        return new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("feedback", feedback)
                .build();
    }

    public static RequestBody ErrorReport(String report) {
        return new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
    }


    public static RequestBody LoginBody(String username, String password) {
        return new FormBody.Builder()
                .addEncoded("userId", username)
                .addEncoded("password", password)
                .build();
    }

    public static RequestBody employeeLogin(String username, String password, String companyId) {
        return new FormBody.Builder()
                .addEncoded("username", username)
                .addEncoded("password", password)
                .addEncoded("companyId", companyId)
                .build();
    }

    public static RequestBody login(String mobileNumber, String macAddr) {
        return new FormBody.Builder()
                .addEncoded("mobileNumber", mobileNumber)
                .addEncoded("macAddr", macAddr)
                .build();
    }

    public static List<KeyValuePair> loginMobile(String mMobile, String otp) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("mobileNumber",mMobile));
        params.add(new KeyValuePair("otp",otp));
        return params;
    }

    public static List<KeyValuePair> updateProfile(String candidateId, String name, String mobileNumber, String address, String pinCode, String latitudeLongitude, String keywords, String imageUrl, String gcmId, String macAddress) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("candidateId",candidateId));
        params.add(new KeyValuePair("shopName",name));
        params.add(new KeyValuePair("pinCode",pinCode));
        params.add(new KeyValuePair("address",address));
        params.add(new KeyValuePair("latitudeLongitude",latitudeLongitude));
        params.add(new KeyValuePair("keywords",keywords));
        params.add(new KeyValuePair("mobileNumber",mobileNumber));
        params.add(new KeyValuePair("gcmId",gcmId));
        params.add(new KeyValuePair("city","NA"));
        params.add(new KeyValuePair("location","NA"));
        params.add(new KeyValuePair("about","NA"));
        params.add(new KeyValuePair("image",imageUrl));
        params.add(new KeyValuePair("macAddr",macAddress));
        return params;
    }

    public static List<KeyValuePair> findAccOwner(String senderId, String keyword, String latitude, String longitude, String radius) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        params.add(new KeyValuePair("keyword",keyword));
        params.add(new KeyValuePair("latitude",latitude));
        params.add(new KeyValuePair("longitude",longitude));
        params.add(new KeyValuePair("radius",radius));
        return params;
    }

    public static List<KeyValuePair> sendNotification(String senderId, String keyword, String message, String receiverIds) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        params.add(new KeyValuePair("keyword",keyword));
        params.add(new KeyValuePair("message",message));
        params.add(new KeyValuePair("receiverIds",receiverIds));
        return params;
    }

    public static List<KeyValuePair> sendChatNotification(String senderId, String keyword, String message, String receiverId, String gcmId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        params.add(new KeyValuePair("keyword",keyword));
        params.add(new KeyValuePair("message",message));
        params.add(new KeyValuePair("receiverId",receiverId));
        params.add(new KeyValuePair("gcmId",gcmId));
        return params;
    }

    public static List<KeyValuePair> connectChat(String senderId, String receiverId, String firebaseId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId",senderId));
        params.add(new KeyValuePair("receiverId",receiverId));
        params.add(new KeyValuePair("firebaseId",firebaseId));
        return params;
    }

    public static List<KeyValuePair> addNewNeed(String senderId, String need) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId", senderId));
        params.add(new KeyValuePair("need", need));
        return params;
    }

    public static List<KeyValuePair> deleteFriend(String senderId, String receiverId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("senderId", senderId));
        params.add(new KeyValuePair("receiverId", receiverId));
        return params;
    }
}
