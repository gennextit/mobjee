package com.gennext.repairshop.util;

import com.gennext.repairshop.model.ChatListModel;
import com.gennext.repairshop.model.ProfileModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class JsonCreator {

    public static String generateProductJSON(ArrayList<String> productList) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            for (String item : productList) {

                JSONObject pnObj = new JSONObject();
                pnObj.put("userId", "01");
                pnObj.put("product", item);
                jsonArr.put(pnObj);
            }
            return jsonArr.toString();
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String generateReceiverIds(ArrayList<ProfileModel> list) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();
            for (ProfileModel item : list) {
                JSONObject pnObj = new JSONObject();
                pnObj.put("candidateId", item.getCandidateId());
                pnObj.put("gcmId", item.getGcmId());
                jsonArr.put(pnObj);
            }
            return jsonArr.toString();
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String generateReceiverId(ChatListModel item) {
        try {
            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();

            JSONObject pnObj = new JSONObject();
            pnObj.put("candidateId", item.getReceiverId());
            pnObj.put("gcmId", item.getGcmId());
            jsonArr.put(pnObj);

            return jsonArr.toString();
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

//    Example
//    {
//        "204":  {
//                "month": "11",
//                "day": "30",
//                 },
//        "202":  {
//                "month": "11",
//                "day": "30",
//                }
//    }

    // parese Json with undefined key name
    private void parseJson(String jsonContent) throws JSONException {
        JSONObject issueObj = new JSONObject(jsonContent);
        Iterator iterator = issueObj.keys();
        while(iterator.hasNext()){
            String key = (String)iterator.next();
            JSONObject issue = issueObj.getJSONObject(key);

            //  get id from  issue
            String _pubKey = issue.optString("id");
        }
    }
    private void parseJson(JSONObject data) {

        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();

                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJson(arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJson(data.getJSONObject(key));
                    } else {
                        System.out.println("" + key + " : " + data.optString(key));
                    }
                } catch (Throwable e) {
                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();

                }
            }
        }
    }

}
