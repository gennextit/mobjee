package com.gennext.repairshop.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONObject;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String COMMON = "mobjee";
    private static final String APP_USER = "appuser" + COMMON;
    private static final String APP_NAME = "app_name" + COMMON;
    private static final String APP_MOBILE = "app_mobile" + COMMON;
    private static final String APP_LATITUDE = "app_latitude" + COMMON;
    private static final String APP_LONGITUDE = "app_longitude" + COMMON;
    private static final String APP_IMAGE = "app_image" + COMMON;
    private static final String APP_FCMID = "app_fcmid" + COMMON;
    private static final String APP_NOTIF = "app_notif" + COMMON;
    private static final String APP_CHAT = "app_chat" + COMMON;


    public static void setLoginData(Context context, JSONObject msgObj) {
        if (context != null && msgObj != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            SharedPreferences.Editor editor = sharedPreferences.edit();
//            if (msgObj.optJSONArray("domains") != null) {
//                editor.putString(STORED_DOMAINS, msgObj.optJSONArray("domains").toString());
//            }else{
//                editor.putString(STORED_DOMAINS, "");
//            }
//            if (msgObj.optJSONArray("days") != null) {
//                editor.putString(STORED_DAYS, msgObj.optJSONArray("days").toString());
//            }else{
//                editor.putString(STORED_DAYS, "");
//            }
//            if (msgObj.optJSONArray("allStatus") != null) {
//                editor.putString(STORED_ALLSTATUS, msgObj.optJSONArray("allStatus").toString());
//            }else{
//                editor.putString(STORED_ALLSTATUS, "");
//            }
//            if (msgObj.optString("consultantId") != null) {
//                editor.putString(CONSULTANT_ID, msgObj.optString("consultantId"));
//            }else{
//                editor.putString(CONSULTANT_ID, "");
//            }

            editor.apply();
        }
    }


    public static String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static String getName(Context activity) {
        return LoadPref(activity, APP_NAME);
    }

    public static void setName(Context context, String name) {
        Utility.SavePref(context, APP_NAME, name);
    }

    public static int getNotifCounter(Context activity) {
        return Utility.LoadPrefInteger(activity, APP_NOTIF);
    }

    public static void setNotifCounter(Context context, int value) {
        Utility.SavePrefInteger(context, APP_NOTIF, value);
    }

    public static int getChatCounter(Context activity) {
        return Utility.LoadPrefInteger(activity, APP_CHAT);
    }

    public static void setChatCounter(Context context, int value) {
        Utility.SavePrefInteger(context, APP_CHAT, value);
    }

    public static void setImage(Context context, String image) {
        Utility.SavePref(context, APP_IMAGE, image);
    }

    public static String getImage(Context activity) {
        return LoadPref(activity, APP_IMAGE);
    }

    public static void setFCMId(Context context, String image) {
        Utility.SavePref(context, APP_FCMID, image);
    }

    public static String getFCMId(Context activity) {
        return LoadPref(activity, APP_FCMID);
    }


    public static String getUserId(Context activity) {
        return LoadPref(activity, APP_USER);
    }

    public static void setUserId(Context context, String value) {
        Utility.SavePref(context, APP_USER, value);
    }

    public static String getLatitude(Context activity) {
        return LoadPref(activity, APP_LATITUDE);
    }

    public static void setLatitude(Context context, String value) {
        Utility.SavePref(context, APP_LATITUDE, value);
    }

    public static String getLongitude(Context activity) {
        return LoadPref(activity, APP_LONGITUDE);
    }

    public static void setLongitude(Context context, String value) {
        Utility.SavePref(context, APP_LONGITUDE, value);
    }

    public static String getMobile(Context activity) {
        return LoadPref(activity, APP_MOBILE);
    }

    public static void setMobile(Context context, String value) {
        Utility.SavePref(context, APP_MOBILE, value);
    }


}