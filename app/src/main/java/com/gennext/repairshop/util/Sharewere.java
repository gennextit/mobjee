package com.gennext.repairshop.util;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.widget.Toast;

// share image through NavDrawer.class method shareClick();
public class Sharewere {


    public static void openExternalBrowser(Activity activity, String url) {
        Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent1);
    }

    public static void rateUs(Activity activity) {
        Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(AppSettings.PLAYSTORE_URL+activity.getPackageName()));
        activity.startActivity(browserIntent1);
    }

    public static void shareApp(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Check out this app! "+AppSettings.PLAYSTORE_URL+activity.getPackageName());
        activity.startActivity(Intent.createChooser(intent, "Share With Fiends"));
    }

    public static void actionCall(Activity activity, String phone) {
        if (!TextUtils.isEmpty(phone)) {
            Uri number = Uri.parse("tel:" + phone);
            Intent callIntent = new Intent(Intent.ACTION_CALL, number);
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            activity.startActivity(callIntent);
        }else{
            Toast.makeText(activity, "Number Not exists", Toast.LENGTH_SHORT).show();
        }
    }

    public static void openExternalNavagation(Activity activity, String lattitude, String longitude, String address) {
        Location location = new Location("local");
        location.setLatitude(Double.parseDouble(lattitude));
        location.setLongitude(Double.parseDouble(longitude));

//                String uri="http://maps.google.com/maps?saddr=" + lattitude
//                        + "," + mCurrentLocation.getLongitude() + "&daddr=" + shopLatLng.latitude + "," + shopLatLng.longitude;
//
//                String strUri = "http://maps.google.com/maps?q=loc:" + shopLatLng.latitude + "," + shopLatLng.longitude + " (" +shopMapModel.getShopName()+"\n"+shopMapModel.getShopAddress() + ")";
//
//                String uri2 = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), "Home Sweet Home", shopLatLng.latitude, shopLatLng.longitude, shopMapModel.getShopName()+"\n"+shopMapModel.getShopAddress());
//
//                String uri3 = "http://maps.google.com/maps?saddr=" + mCurrentLocation.getLatitude() + "," +
//                        mCurrentLocation.getLongitude() + "(" + "Home Sweet Home" + ")&daddr=" +
//                        shopLatLng.latitude + "," + shopLatLng.longitude + " (" + "Where the party is at" + ")";

        // Display a label at the location of Google's Sydney office
        String gmmIntentUri = "geo:0,0?q=" + lattitude + "," + longitude + "(" + address + ")";

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(gmmIntentUri));
//        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        intent.setPackage("com.google.android.apps.maps");
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException ex) {

            Toast.makeText(activity, "Please install a maps application", Toast.LENGTH_LONG).show();

        }
    }
}
