package com.gennext.repairshop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import com.gennext.repairshop.global.PopupProgress;
import com.gennext.repairshop.model.chat.ChatModel;
import com.gennext.repairshop.model.chat.FileModel;
import com.gennext.repairshop.model.chat.MapModel;
import com.gennext.repairshop.user.profile.Profile;
import com.gennext.repairshop.util.FileUtils;
import com.gennext.repairshop.util.L;
import com.gennext.repairshop.util.Util;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class ProfileActivity extends BaseActivity {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;

    private static final int PLACE_PICKER_REQUEST = 3;
    //File
    private Uri filePathImageCameraUri;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String TAG = "ProfileActivity";
    private AlertDialog pDialog;
    private String mPhotoStringLink;
    private Profile profile;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profile =Profile.newInstance();
        addFragmentWithoutBackstack(profile,"profile");

//        verifyStoragePermissions();
    }


    public void OptionImageAlert(Activity Act) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        LayoutInflater inflater = Act.getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_image_option, null);
        dialogBuilder.setView(v);
        ((Button) v.findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                verifyStoragePermissions();

            }
        });
        ((Button) v.findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                photoGalleryIntent();
            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }


    public void openLocationPicker() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_PROFILE);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
//                    sendFileFirebase(storageRef, selectedImageUri);
                    startCropImageActivity(selectedImageUri);
                } else {
                    //URI IS NULL
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                String imageName = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
//                filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName + "camera.jpg");
//                filePathImageCameraUri = FileProvider.getUriForFile(ProfileActivity.this,
//                        BuildConfig.APPLICATION_ID + ".provider",
//                        filePathImageCamera);
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                filePathImageCameraUri = FileUtils.getImageUri(ProfileActivity.this,imageBitmap,imageName);
                if (filePathImageCameraUri!=null) {
//                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() + "_camera");
//                    sendFileFirebase(imageCameraRef, filePathImageCamera);
                    startCropImageActivity(filePathImageCameraUri);
                }
            }
        }else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                sendFileFirebase(storageRef, result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: please select a valid Image" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }else if (requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place!=null){
                    LatLng latLng = place.getLatLng();
                    MapModel mapModel = new MapModel(latLng.latitude+"",latLng.longitude+"", place.getName()!=null ?place.getName().toString() :"Name not available");

                    L.m(mapModel.getLatitude());
                    Profile profile = (Profile) getSupportFragmentManager().findFragmentByTag("profile");
                    if(profile!=null){
                        profile.setLocation(mapModel);
                    }
                }else{
                    L.m("Getting error on place picker");
                }
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        if(imageUri!=null) {
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }else {
            Toast.makeText(ProfileActivity.this, "Please Reselect image ", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {
            pDialog = PopupProgress.newInstance(ProfileActivity.this).show();
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    pDialog.dismiss();
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
//                        String downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            pDialog.dismiss();
                            mPhotoStringLink = uri.toString();
                            if(profile!=null){
                                profile.updateImagePath(uri.toString());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pDialog.dismiss();
                        }
                    });

                }
            });

        } else {
            //IS NULL
        }

    }
    private void sendFileFirebase(StorageReference storageReference, File file) {
        if (storageReference != null) {
            pDialog = PopupProgress.newInstance(ProfileActivity.this).show();
            Uri photoURI = FileProvider.getUriForFile(ProfileActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    pDialog.dismiss();
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            pDialog.dismiss();
                            mPhotoStringLink = uri.toString();
                            if(profile!=null){
                                profile.updateImagePath(uri.toString());
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pDialog.dismiss();
                        }
                    });

                }
            });
        }
    }


    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }



    private void photoCameraIntent() {
//        String imageName = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
//        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), imageName + "camera.jpg");
//        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        filePathImageCameraUri = FileProvider.getUriForFile(ProfileActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                filePathImageCamera);
//        it.putExtra(MediaStore.EXTRA_OUTPUT, filePathImageCameraUri);
//        if (it.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(it, IMAGE_CAMERA_REQUEST);
//        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
        }
    }
//    //WithoutPath
//    private void photoCameraIntent() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
//        }
//    }

    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    ProfileActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            // we already have permission, lets go ahead and call camera intent
            photoCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    photoCameraIntent();
                }
                break;
        }
    }

}
