package com.gennext.repairshop.fcm;

/**
 * Created by Admin on 8/16/2017.
 */

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.gennext.repairshop.MainActivity;
import com.gennext.repairshop.R;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.DBHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String NOTI_RESULT_CODE = "NotiResultCode";
    private static final String TAG = "MyFirebaseMsgService";
    public static final String ACTION = "com.gennext.repairshop.fcm.MyFirebaseMessagingService";
    private static final int NOTIFICATION_SEARCH = 1001;
    private Notification.Builder builder;

    public static String chattingUserId;
    private DBHelper dbHelper;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        AppUser.setFCMId(getApplicationContext(), token);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        dbHelper = new DBHelper(getApplicationContext());
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload. Received from MySqlServer
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }
            try {
                JSONObject noti = new JSONObject(remoteMessage.getData());
                String notifBody = noti.optString("notification");
                if (!TextUtils.isEmpty(notifBody)) {
                    String[] detail = parseMessage(notifBody);
                    if (detail != null) {
                        String senderId = detail[0];
                        String senderName = detail[1];
                        String keyword = detail[2];
                        String message = detail[3];

                        String finalTitle, finalBody;
                        if (keyword.equalsIgnoreCase(Const.REPLIED_CHAT)) {
                            if (!senderId.equalsIgnoreCase(chattingUserId)) {// Chat user not matched
                                finalTitle = Const.getStatusTitleForChat(senderName);
                                finalBody = Const.getStatusBodyForChat(message);
                                dbHelper.insertChatCounter(AppUser.getUserId(getApplicationContext()), senderId, 1);
                                showNotification(Const.REPLIED_CHAT, senderName, finalTitle, finalBody);
                                sendBroadcastMessage(Const.REPLIED_CHAT);
                            }
                        } else {
                            finalTitle = Const.getStatusTitle(senderId, senderName, keyword);
                            finalBody = Const.getStatusBody(message);
                            showNotification(Const.REPLIED_NOTIFICATION, senderName, finalTitle, finalBody);
                            sendBroadcastMessage(Const.REPLIED_NOTIFICATION);
                        }
                    } else {
                        showNotification(Const.REPLIED_NOTIFICATION, "Error", "Contact App provider", "Error: Notification not in correct format.");
                    }
                } else {
                    showNotification(Const.REPLIED_NOTIFICATION, "Error", "Contact App provider", "Notification body is empty");
                }
            } catch (Exception e) {
                showNotification(Const.REPLIED_NOTIFICATION, "Error", "Contact App provider", "Json exception occurred");
            }
        }

        // Check if message contains a notification payload. Received from Firebase Pannel
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            showNotification(Const.REPLIED_NOTIFICATION, "Firebase Pannel", "Message", remoteMessage.getNotification().getBody());
        }

    }

    private String[] parseMessage(String notifBody) {
        try {
            return notifBody.split("\\|");
        } catch (Exception e) {
            return null;
        }
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    private void showNotificationOld(String from, String senderName, String finalTitle, String finalBody) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Const.FROM, from);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);


        builder = new Notification.Builder(this);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_stat_name);
        builder.setAutoCancel(true);//// running on background with false param
        builder.setContentTitle(finalTitle);
        builder.setContentText(finalBody);
        builder.setTicker(Const.getTikerNotification(senderName));
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//        builder.setNumber(notificationBageCount);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            builder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
//        }
//        builder.setSubText("Tap to view the website.");

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//        // Will display the notification in the notification bar
        notificationManager.notify(NOTIFICATION_SEARCH, builder.build());
//        startForeground(NOTIFICATION_SEARCH, builder.build());// running on background
    }

    public void showNotification(String from, String senderName, String finalTitle, String finalBody) {
        Context context = getApplicationContext();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Const.FROM, from);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(finalTitle)
                .setContentText(finalBody)
                .setAutoCancel(true)
                .setTicker(Const.getTikerNotification(senderName))
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(finalBody));

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NOTIFICATION_SEARCH, mBuilder.build());
    }

    private void sendBroadcastMessage(String from) {
        int counter;
//        if(from.equals(Const.REPLIED_CHAT)) {
//            counter = AppUser.getChatCounter(getApplicationContext());
//            AppUser.setChatCounter(getApplicationContext(), counter + 1);
//        } else {
        if (!from.equals(Const.REPLIED_CHAT)) {
            counter = AppUser.getNotifCounter(getApplicationContext());
            AppUser.setNotifCounter(getApplicationContext(), counter + 1);
        }
        setBadgeCount();
        Intent in = new Intent(ACTION);
        in.putExtra(NOTI_RESULT_CODE, Activity.RESULT_OK);
        in.putExtra(Const.FROM, from);
        LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(in);
    }

    private void setBadgeCount() {
        int notificationBageCount = dbHelper.getChatCounter(getApplicationContext()) + AppUser.getNotifCounter(getApplicationContext());
        setBadge(getApplicationContext(),notificationBageCount);
//        try {
//            ShortcutBadger.applyCountOrThrow(getApplicationContext(), notificationBageCount); //for 1.1.4+
//        } catch (ShortcutBadgeException e) {
//            e.printStackTrace();
//        }

    }
    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }
//
//    private void sendNotification(String messageBody) {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.putExtra(Const.FROM,Const.NOTIFICATION);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.ic_about)
//                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                        R.mipmap.ic_launcher))
//                .setContentTitle("Mobji")
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//    }

    private void updateNotification() {

        int api = Build.VERSION.SDK_INT;
        //update action
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("logoutcheck", "");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle("Title");
//        if(isDebugMode) {
//            builder.setContentText("Tracking started: " + count++);
//        }else{
        builder.setContentText("Body message");
        // update the notification
        if (api >= Build.VERSION_CODES.HONEYCOMB) {
            startForeground(NOTIFICATION_SEARCH, builder.build());
//            notificationManager.notify(NOTIFICATION_GPS, builder.build());
        }
    }
}
