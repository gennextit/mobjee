package com.gennext.repairshop;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.repairshop.fcm.MyFirebaseMessagingService;
import com.gennext.repairshop.global.PopupDialog;
import com.gennext.repairshop.search.ChatSearchBar;
import com.gennext.repairshop.search.SearchBar;
import com.gennext.repairshop.setting.Const;
import com.gennext.repairshop.user.Dashboard;
import com.gennext.repairshop.user.Need;
import com.gennext.repairshop.user.chat.ChatList;
import com.gennext.repairshop.user.notification.Notification;
import com.gennext.repairshop.user.profile.MyKeywords;
import com.gennext.repairshop.util.AppUser;
import com.gennext.repairshop.util.DBHelper;
import com.gennext.repairshop.util.L;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;


public class MainActivity extends NavDrawer {

    private Toolbar mToolbar;
    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //startActivity(new Intent(getActivity(),NewLeadActivity.class));
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(getApplicationContext(), getString(R.string.permission_denied) + "\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };
    private FrameLayout searchContainer,searchChatContainer;
    private BottomNavigationView navigation;
    private Need needFrag;
    private Dashboard dashboardFrag;
    private Notification notificationFrag;
    private ChatList chatListFrag;
    private TextView tvNotifCounter, tvChatCounter;
    private int tabSelected;
    private DBHelper dbHelper;
//    private MenuItem addItem;

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_menu, menu);
//        addItem = menu.findItem(R.id.menu_action_add);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // action with ID action_refresh was selected
//            case R.id.menu_action_add:
//                Need need = (Need) getSupportFragmentManager().findFragmentByTag("need");
//                if(need!=null){
//                    need.addNewFeed();
//                }
//                break;
//            default:
//                break;
//        }
//
//        return true;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DBHelper(this);
        MyFirebaseMessagingService.chattingUserId = "";
        mToolbar = setToolBar(getSt(R.string.app_name));
        setAboutDetail();
        tvNotifCounter = (TextView) findViewById(R.id.tv_notification_counter);
        tvChatCounter = (TextView) findViewById(R.id.tv_chat_counter);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        searchContainer = (FrameLayout) findViewById(R.id.container_bar);
        searchChatContainer = (FrameLayout) findViewById(R.id.container_bar_chat);
        setSupportActionBar(mToolbar);
        SetDrawer(this, mToolbar);
        needFrag = Need.newInstance();
        dashboardFrag = Dashboard.newInstance();
        notificationFrag = Notification.newInstance();
        chatListFrag = ChatList.newInstance();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                hideKeybord(MainActivity.this);
                MyFirebaseMessagingService.setBadge(MainActivity.this, 0); //for 1.1.4+
                switch (item.getItemId()) {
                    case R.id.navigation_menu_0:
                        tabSelected = R.id.navigation_menu_0;
                        mToolbar.setTitle("Needs");
                        setTabFragment(needFrag, R.id.container_main, "need", dashboardFrag, chatListFrag, notificationFrag);
                        if (needFrag != null && needFrag.isAdded()) {
                            needFrag.refreshList();
                        }
                        searchContainer.setVisibility(View.GONE);
                        searchChatContainer.setVisibility(View.GONE);
                        return true;
                    case R.id.navigation_menu_1:
                        tabSelected = R.id.navigation_menu_1;
                        mToolbar.setTitle("Search");
                        setTabFragment(dashboardFrag, R.id.container_main, "dashboard", needFrag, chatListFrag, notificationFrag);
                        searchContainer.setVisibility(View.VISIBLE);
                        searchChatContainer.setVisibility(View.GONE);
                        return true;
                    case R.id.navigation_menu_2:
                        tabSelected = R.id.navigation_menu_2;
                        mToolbar.setTitle("Notification");
                        setTabFragment(notificationFrag, R.id.container_main, "notification", needFrag, dashboardFrag, chatListFrag);
                        if (notificationFrag != null && notificationFrag.isAdded()) {
                            notificationFrag.refreshList();
                        }
                        AppUser.setNotifCounter(MainActivity.this, 0);
                        setBadgeCounter(Const.REPLIED_NOTIFICATION);
                        searchContainer.setVisibility(View.GONE);
                        searchChatContainer.setVisibility(View.GONE);
                        return true;
                    case R.id.navigation_menu_3:
                        tabSelected = R.id.navigation_menu_3;
                        mToolbar.setTitle("Chat");
                        setTabFragment(chatListFrag, R.id.container_main, "chatList", needFrag, dashboardFrag, notificationFrag);
                        if (chatListFrag != null && chatListFrag.isAdded()) {
                            chatListFrag.refreshList();
                        }
                        AppUser.setChatCounter(MainActivity.this, 0);
                        setBadgeCounter(Const.REPLIED_CHAT);
                        searchContainer.setVisibility(View.GONE);
                        searchChatContainer.setVisibility(View.VISIBLE);
                        return true;
                }
                return false;
            }
        });
        initUi(getIntent());

        addSearchTag(savedInstanceState);
        addSearchChatTag(savedInstanceState);
        checkPermission();
    }


    public void showSearchBar() {
        searchContainer.setVisibility(View.VISIBLE);
    }

    private void addSearchTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = SearchBar.newInstance(new SearchBar.Search() {
                @Override
                public void onTextChanged(CharSequence charSequence) {

                }
            });
//          AppAnimation.setSlideAnimation(fragment, Gravity.TOP);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_bar, fragment, "searchBar")
                    .commit();
        }
    }

    private void addSearchChatTag(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragment = ChatSearchBar.newInstance(new ChatSearchBar.ChatSearch() {
                @Override
                public void onTextChanged(CharSequence charSequence) {
                    if(chatListFrag!=null){
                        chatListFrag.onSearchTextChanged(charSequence);
                    }
                }
            });
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_bar_chat, fragment, "chatSearchBar")
                    .commit();
        }
    }


    private void checkPermission() {
        new TedPermission(getApplicationContext())
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.if_you_reject_permission_you_can_not_use_this_service_please_turn_on_permissions))
                .setPermissions(Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }


    public void updateTitle(String title) {
        mToolbar.setTitle(title);
    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

        if (backStackEntryCount == 1) {
//            updateTitle("Dashboard");
//            Dashboard dashboard= (Dashboard) getSupportFragmentManager().findFragmentByTag("dashboard");
//            if(dashboard!=null){
//                dashboard.updateUi();
//            }
        }
        if (backStackEntryCount > 0) {
            MyKeywords myKeywords = (MyKeywords) getSupportFragmentManager().findFragmentByTag("myKeywords");
            if (myKeywords != null) {
                if (myKeywords.mItemAddedInList) {
                    PopupDialog.newInstance("Alert!", "Are you sure to exit without saving keywords?", "Yes", "No", new PopupDialog.DialogListener() {
                        @Override
                        public void onOkClick(DialogFragment dialog) {
                            getSupportFragmentManager().popBackStack();
                        }

                        @Override
                        public void onCancelClick(DialogFragment dialog) {

                        }
                    }).show(getSupportFragmentManager(), "popupDialog");
                } else {
                    getSupportFragmentManager().popBackStack();
                }
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void initUi(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            String from = intent.getExtras().getString(Const.FROM);
            if (from != null && from.equalsIgnoreCase(Const.REPLIED_CHAT)) {
                navigation.setSelectedItemId(R.id.navigation_menu_3);
                tabSelected = R.id.navigation_menu_3;
            } else {
                navigation.setSelectedItemId(R.id.navigation_menu_2);
                tabSelected = R.id.navigation_menu_2;
            }
        } else {
            navigation.setSelectedItemId(R.id.navigation_menu_0);
            tabSelected = R.id.navigation_menu_0;
        }
    }


    // Broadcast Reviver

    @Override
    public void onResume() {
        super.onResume();
        L.m("The onResume() event");
        IntentFilter filter = new IntentFilter(MyFirebaseMessagingService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
        initialNotificationCounter();
        removeBatchCounter();
    }

    private void removeBatchCounter() {
        try {
            MyFirebaseMessagingService.setBadge(MainActivity.this, 0); //for 1.1.4+
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        L.m("The onPause() event");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra(MyFirebaseMessagingService.NOTI_RESULT_CODE, RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                String from = intent.getStringExtra(Const.FROM);
                refreshNotificationList(from);
            }
            removeBatchCounter();
        }
    };

    private void refreshNotificationList(String from) {
        if (from.equals(Const.REPLIED_CHAT)) {
            if (chatListFrag != null && tabSelected == R.id.navigation_menu_3) {
                chatListFrag.refreshList();
                AppUser.setChatCounter(getApplicationContext(), 0);
            }
            setBadgeCounter(from);
        } else {
            if (notificationFrag != null && tabSelected == R.id.navigation_menu_2) {
                notificationFrag.refreshList();
                AppUser.setNotifCounter(getApplicationContext(), 0);
            } else {
                setBadgeCounter(from);
            }
        }
    }

    public void setBadgeCounter(String from) {
        if (from.equals(Const.REPLIED_CHAT)) {
            int counter = dbHelper.getChatCounter(MainActivity.this);
            if (counter == 0) {
                tvChatCounter.setVisibility(View.GONE);
            } else {
                tvChatCounter.setVisibility(View.VISIBLE);
                tvChatCounter.setText(String.valueOf(counter));
            }
        } else {
            int counter = AppUser.getNotifCounter(MainActivity.this);
            if (counter == 0) {
                tvNotifCounter.setVisibility(View.GONE);
            } else {
                tvNotifCounter.setVisibility(View.VISIBLE);
                tvNotifCounter.setText(String.valueOf(counter));
            }
        }
    }

    public void initialNotificationCounter() {
        int counter = dbHelper.getChatCounter(MainActivity.this);
        if (counter == 0) {
            tvChatCounter.setVisibility(View.GONE);
        } else {
            tvChatCounter.setVisibility(View.VISIBLE);
            tvChatCounter.setText(String.valueOf(counter));
        }
        counter = AppUser.getNotifCounter(MainActivity.this);
        if (counter == 0) {
            tvNotifCounter.setVisibility(View.GONE);
        } else {
            tvNotifCounter.setVisibility(View.VISIBLE);
            tvNotifCounter.setText(String.valueOf(counter));
        }
    }


}
